/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.nodejs

import org.ysb33r.gradle.nodejs.testfixtures.UnittestBaseSpecification
import org.ysb33r.grolifant5.api.core.ProjectOperations

class NpmScriptExecSpecSpec extends UnittestBaseSpecification {

    void 'Can configure a script'() {
        setup:
        final po = ProjectOperations.find(project)
        final nodejs = project.extensions.getByType(NodeJSExtension)
        final npm = project.extensions.getByType(NpmExtension)
        final npmExecSpec = new NpmScriptExecSpec(nodejs.executable, npm.executable, po)
        final execSpec = po.execTools.execSpec()

        when:
        npmExecSpec.script {
            name = 'test'
        }
        npmExecSpec.copyTo(execSpec)

        then:
        execSpec.args.contains('--scripts-prepend-node-path=true')
        execSpec.argumentProviders*.asArguments().flatten().contains('test')
    }
}