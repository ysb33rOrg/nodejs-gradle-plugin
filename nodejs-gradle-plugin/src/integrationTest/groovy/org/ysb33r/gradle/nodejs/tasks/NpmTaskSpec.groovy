/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.nodejs.tasks

import org.ysb33r.gradle.nodejs.testfixtures.IntegrationSpecification
import spock.lang.Issue
import spock.lang.Unroll

import static org.gradle.testkit.runner.TaskOutcome.SUCCESS

class NpmTaskSpec extends IntegrationSpecification {

    @Unroll
    void 'Install a package #cc configuration cache'() {
        setup:
        final taskName = 'installStringz'
        final node_modules = new File(projectDir, 'node_modules')
        writeBuildFile()
        buildFile << """
        tasks.register('${taskName}', NpmTask) {
            cmd {
               command = 'install'
               args 'stringz@0.2.2'
            }
        }
        """.stripIndent()

        when:
        final result = (ccMode ? getGradleRunnerConfigCache(IS_GROOVY_DSL, projectDir, [taskName, '-s']) :
            getGradleRunner(IS_GROOVY_DSL, projectDir, [taskName])).build()

        then:
        result.task(":${taskName}").outcome == SUCCESS
        node_modules.exists()
        new File(node_modules, 'stringz').exists()

        where:
        cc        | ccMode
        'without' | false
        'with'    | true
    }

    @Issue('https://gitlab.com/ysb33rOrg/gradle/nodejs-gradle-plugin/-/issues/53')
    void 'NpmTask should pass correct environment'() {

        setup:
        final taskName = 'npmEnvironment'
        writeBuildFile()
        buildFile << """
        npm {
            localConfig = useLocalProjectLocation()
        }
        
        tasks.register('${taskName}', NpmTask) {
            cmd {
                command = 'run'
                args = ["environment"]
            }
        }
        """.stripIndent()

        new File(projectDir, 'package.json').text = '''
        {
          "scripts": {
            "environment": "printenv"
          }
        }
        '''.stripIndent()

        when:
        final result = getGradleRunner(IS_GROOVY_DSL, projectDir, [taskName, '-s', '-i']).build()

        then:
        !result.output.readLines().find { it.matches(~/.*extension 'npm' property.*/) }
    }

    private void writeBuildFile() {
        buildFile.text = """
        import org.ysb33r.gradle.nodejs.tasks.NpmTask

        plugins {
            id  'org.ysb33r.nodejs.npm'
        }
        
        nodejs {
          executableByVersion('${NODEJS_VERSION}')
        }
        """.stripIndent()
    }
}