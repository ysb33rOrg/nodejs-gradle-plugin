/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.nodejs.plugins

import org.ysb33r.gradle.nodejs.testfixtures.IntegrationSpecification
import spock.lang.Unroll

import static org.ysb33r.gradle.nodejs.PackageJson.PACKAGE_JSON

class NpmDevPluginSpec extends IntegrationSpecification {
    File packageJson

    void setup() {
        buildFile.text = """
        plugins {
            id 'org.ysb33r.nodejs.dev'
            id 'org.ysb33r.nodejs.wrapper'
        }
        
        nodejs {
            executableByVersion('${NODEJS_VERSION}')
            useSystemPath()
        }
        """.stripIndent()

        packageJson = new File(projectDir, "src/node/${PACKAGE_JSON}")
        packageJson.parentFile.mkdirs()
        packageJson.text = '''{
            "name": "5.3.1",
            "version": "1.0.1",
            "license": "ISC",
            "devDependencies": {
                "@antora/cli": "2.2",
                "@antora/site-generator-default": "2.2"
            },
            "main": "app/index.js",
            "scripts": {
                "test": "npx --help > test.txt"
            }
        }
        '''.stripIndent()
    }

    @Unroll
    void 'Load correct version of project on Gradle #ver'() {
        setup:
        final checkVersion = 'checkVersion'
        def args = ['-i', '-s', 'packageJsonInstall', checkVersion]

        buildFile << """
        npm {
           usePackageJsonVersionAsProjectVersion()
        }

        task ${checkVersion} {
            doLast {
                assert project.version.toString() == '1.0.1'
            }
        }
        """.stripIndent()

        when:
        getGradleRunner(IS_GROOVY_DSL, projectDir, args).withGradleVersion(ver).build()

        then:
        noExceptionThrown()

        where:
        ver << ['7.3.3', '8.9']
    }

}