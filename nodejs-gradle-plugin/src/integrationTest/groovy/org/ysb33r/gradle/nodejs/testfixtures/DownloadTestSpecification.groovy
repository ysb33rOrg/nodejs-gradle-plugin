/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.nodejs.testfixtures

import org.ysb33r.grolifant5.api.core.OperatingSystem
import spock.lang.Specification

@SuppressWarnings('LineLength')
class DownloadTestSpecification extends Specification {
    public static final String NODEJS_VERSION = System.getProperty('NODEJS_DEFAULT_VERSION')
    public static final String NODEJS_DOWNLOAD_SYSTEM_PROPERTY = "-Dorg.ysb33r.gradle.nodejs.uri=${System.getProperty('org.ysb33r.gradle.nodejs.uri')}"
    public static final List<String> NODEJS_OTHER_VERSIONS = System.getProperty('NODEJS_OTHER_VERSIONS').split(',').toList().asImmutable()
    public static final File RESOURCES_DIR = new File(System.getProperty('RESOURCES_DIR') ?: './src/integrationTest/resources')

    public static final OperatingSystem OS = OperatingSystem.current()
    public static final boolean SKIP_TESTS = !(OS.macOsX || OS.linux || OS.windows)

    public static final File TEST_TMP_DIR
    public static final File TESTKIT_DIR

    static {
        TEST_TMP_DIR = new File(System.getProperty(
            'TEST_TMP_DIR',
            "${System.getProperty('java.io.tmpdir')}/nodejs-gradle-plugin")
        )
        TESTKIT_DIR = new File(System.getProperty('TESTKIT_DIR', './build/tmp/integrationTest/testkit'))
        TESTKIT_DIR.mkdirs()
    }
}