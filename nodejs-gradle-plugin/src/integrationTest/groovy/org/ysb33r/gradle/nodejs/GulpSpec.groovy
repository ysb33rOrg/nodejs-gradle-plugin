/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.nodejs

import org.ysb33r.gradle.nodejs.testfixtures.IntegrationSpecification
import spock.lang.Issue
import spock.lang.Unroll

import static org.ysb33r.gradle.nodejs.plugins.DevBasePlugin.INSTALL_TASK

class GulpSpec extends IntegrationSpecification {

    public static final String TEST_TASK = 'runTest'

    @Issue('https://gitlab.com/ysb33rOrg/gradle/nodejs-gradle-plugin/-/issues/53')
    @Unroll
    void 'Can run a Gulp project #ccMode'() {
        setup:
        writeBuildFile()
        createGulpProject()
        final args = [
            TEST_TASK,
            '-i',
            '-s'
        ]*.toString()

        when:
        final result = (withCC ? getGradleRunnerConfigCache(IS_GROOVY_DSL, projectDir, args) :
            getGradleRunner(IS_GROOVY_DSL, projectDir, args)
        ).build()

        then:
        !result.output.contains('provider')

        where:
        ccMode                        | withCC
        'without configuration cache' | false
        'with configuration cache'    | true
    }

    private void writeBuildFile() {
        buildFile.text = """
        import org.ysb33r.gradle.nodejs.tasks.GulpTask
        import org.ysb33r.gradle.nodejs.tasks.NpmPackageJsonInstall

        plugins {
            id 'org.ysb33r.nodejs.gulp'
        }
        
        tasks.register('${INSTALL_TASK}', NpmPackageJsonInstall) {
        }
        
        tasks.register('${TEST_TASK}', GulpTask) {
            dependsOn '${INSTALL_TASK}'
            target 'printEnvironment'
        }
        """.stripIndent()
    }

    private void createGulpProject() {
        new File(projectDir, 'gulpfile.js').text = '''
        const gulp = require('gulp');
        
        gulp.task('printEnvironment', function(done) {
            console.log(process.env)
            done()
        })
        '''.stripIndent()

        new File(projectDir, 'package.json').text = '''
        {
          "devDependencies": {
            "gulp": "^4.0.2"
          }
        }
        '''.stripIndent()
    }
}