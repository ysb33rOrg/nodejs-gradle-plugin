/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.nodejs.testfixtures

class NpmIntegrationTestSpecification extends IntegrationSpecification {

    void setup() {
        buildFile.text = """
        plugins {
            id 'org.ysb33r.nodejs.npm'
        }
        
        nodejs {
            executableByVersion('${NODEJS_VERSION}')
        }
        
        npm {
        }
        """
    }
}