/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.nodejs.internal

import org.gradle.api.Project
import org.gradle.testfixtures.ProjectBuilder
import org.ysb33r.gradle.nodejs.NodeJSExtension
import org.ysb33r.gradle.nodejs.NpmExtension
import org.ysb33r.gradle.nodejs.testfixtures.DownloadTestSpecification
import org.ysb33r.gradle.nodejs.internal.npm.NpmInstaller
import org.ysb33r.grolifant5.api.core.ConfigCacheSafeOperations
import org.ysb33r.grolifant5.api.core.ProjectOperations

class NpmInstallerSpec extends DownloadTestSpecification {
    public static final String NPM_TEST_VERSION = '8.1.0'
    public static final String NPM_2ND_TEST_VERSION = '7.24.2'

    Project project = ProjectBuilder.builder().build()
    ProjectOperations projectOperations

    NpmInstaller customNpmInstaller

    void setup() {
        project.allprojects {
            apply plugin: 'org.ysb33r.nodejs.npm'

            nodejs {
                executableByVersion(DownloadTestSpecification.NODEJS_VERSION)
            }
        }
        projectOperations = ProjectOperations.find(project)
        customNpmInstaller = new NpmInstaller(
            ConfigCacheSafeOperations.from(project),
            project.extensions.getByType(NodeJSExtension),
            project.extensions.getByType(NpmExtension)
        )
    }

    void 'Can install a custom version'() {
        setup:
        File check = new File(project.npm.homeDirectory, 'node_modules/npm/docs/output/commands/npm.html')
        File location = new File(project.npm.homeDirectory, 'node_modules/npm/bin/npm-cli.js')

        when:
        File firstInstall = customNpmInstaller.getByVersion(NPM_TEST_VERSION)

        then:
        location.exists()
        firstInstall == location
        check.text.contains("<p>${NPM_TEST_VERSION}")

        when:
        File secondInstall = customNpmInstaller.getByVersion(NPM_2ND_TEST_VERSION)

        then:
        secondInstall == location
        check.text.contains("<p>${NPM_2ND_TEST_VERSION}")
    }
}