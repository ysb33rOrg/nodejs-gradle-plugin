/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.nodejs.tasks

import org.gradle.testkit.runner.TaskOutcome
import org.ysb33r.gradle.nodejs.testfixtures.DownloadTestSpecification
import org.ysb33r.gradle.nodejs.testfixtures.IntegrationSpecification

import static org.ysb33r.gradle.nodejs.plugins.DevBasePlugin.SYNC_NVMRC_TASK

class SyncProjectToNvmRcSpec extends IntegrationSpecification {

    void 'Version is synced'() {
        setup:
        writeBuildFile()
        final nvmrc = new File(projectDir, 'src/node/.nvmrc')

        when:
        final failure = getGradleRunner(IS_GROOVY_DSL, projectDir, [SYNC_NVMRC_TASK]).buildAndFail()

        then:
        failure.output.contains('.nvmrc (No such file or directory)')

        when:
        nvmrc.parentFile.mkdirs()
        final result = getGradleRunner(IS_GROOVY_DSL, projectDir, SYNC_NVMRC_TASK).build()

        then:
        result.task(":${SYNC_NVMRC_TASK}").outcome == TaskOutcome.SUCCESS
        nvmrc.text == DownloadTestSpecification.NODEJS_VERSION

        when:
        final resultCC = getGradleRunnerConfigCache(IS_GROOVY_DSL, projectDir, [SYNC_NVMRC_TASK]).build()

        then:
        resultCC.task(":${SYNC_NVMRC_TASK}").outcome == TaskOutcome.SUCCESS
    }

    private void writeBuildFile() {
        buildFile.text = """
        plugins {
            id 'org.ysb33r.nodejs.dev.base'
        }
        """.stripIndent()
    }
}