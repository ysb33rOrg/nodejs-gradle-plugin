/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.nodejs

import org.gradle.api.Project
import org.gradle.testfixtures.ProjectBuilder
import org.ysb33r.gradle.nodejs.plugins.NpmPlugin
import org.ysb33r.gradle.nodejs.testfixtures.DownloadTestSpecification
import org.ysb33r.gradle.nodejs.utils.npm.NpmExecutor
import org.ysb33r.grolifant5.api.core.ConfigCacheSafeOperations
import org.ysb33r.grolifant5.api.core.ProjectOperations

class NpmDependencySpec extends DownloadTestSpecification {

    Project project = ProjectBuilder.builder().build()
    ProjectOperations po

    void setup() {
        po = ProjectOperations.maybeCreateExtension(project)
    }

    void 'Declare a dependency and resolve it'() {
        given:
        project.allprojects {
            apply plugin: 'org.ysb33r.nodejs.npm'

            nodejs {
                executableByVersion(DownloadTestSpecification.NODEJS_VERSION)
            }

            // tag::declare-npm-dependency[]
            dependencies {
                pnpm npm.pkg(name: 'stringz', tag: '0.2.2')
            }
            // end::declare-npm-dependency[]
        }

        when:
        new NpmExecutor(
            ConfigCacheSafeOperations.from(po),
            project.extensions.nodejs,
            project.extensions.npm
        ).initPkgJson(
            'tst',
            po.provider { -> '1.0.0 ' }
        )
        Set<File> files = project.configurations.getByName(NpmPlugin.CONFIGURATION_NAME).resolve()

        then:
        files.size() > 0
    }

    void 'Declare a dependency which requires a shell to install'() {
        def packageProperties = [name: 'puppeteer', tag: '19.4.0']

        project.allprojects {
            apply plugin: 'org.ysb33r.nodejs.npm'

            nodejs {
                executableByVersion(DownloadTestSpecification.NODEJS_VERSION)
                useSystemPath()
            }

            dependencies {
                pnpm npm.pkg(packageProperties)
            }
        }

        when:
        new NpmExecutor(
            ConfigCacheSafeOperations.from(po),
            project.extensions.nodejs,
            project.extensions.npm
        ).initPkgJson(
            'tst',
            po.provider { -> '1.0.0 ' }
        )
        Set<File> files = project.configurations.getByName(NpmPlugin.CONFIGURATION_NAME).resolve()

        then:
        files.size() > 0
    }
}