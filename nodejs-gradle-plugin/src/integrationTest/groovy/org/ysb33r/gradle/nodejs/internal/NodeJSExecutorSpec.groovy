/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.nodejs.internal

import org.gradle.api.Project
import org.ysb33r.gradle.nodejs.NodeJSExecSpec
import org.ysb33r.gradle.nodejs.NodeJSExtension
import org.ysb33r.gradle.nodejs.NpmExtension
import org.ysb33r.gradle.nodejs.testfixtures.NpmBaseTestSpecification
import org.ysb33r.gradle.nodejs.utils.npm.NpmExecutor
import org.ysb33r.grolifant5.api.core.ConfigCacheSafeOperations
import org.ysb33r.grolifant5.api.core.runnable.ExecOutput

import static org.ysb33r.grolifant5.api.core.ExecTools.OutputType.FORWARD
import static org.ysb33r.grolifant5.api.core.ExecTools.OutputType.FORWARD_AND_CAPTURE

class NodeJSExecutorSpec extends NpmBaseTestSpecification {

    NpmExecutor npmExecutor

    void setup() {
        project.extensions.getByType(NodeJSExtension).useSystemPath()

        npmExecutor = new NpmExecutor(
            ConfigCacheSafeOperations.from(project),
            project.extensions.nodejs,
            project.extensions.npm
        )
        npmExecutor.initPkgJson('testproject', projectOperations.versionProvider)
    }

    void 'Download a file then execute it'() {
        setup:
        File gulpFile = setupGulpDownload(project)
        NpmExtension npm = (NpmExtension) (project.extensions.npm)
        NodeJSExtension nodejs = (NodeJSExtension) (project.extensions.nodejs)

        when:
        NodeJSExecSpec execSpec = nodejs.createExecSpec()
        execSpec.script { spec ->
            spec.path = gulpFile
            spec.args '--help'
        }
        execSpec.entrypoint {
            it.workingDir(npm.homeDirectoryProvider)
        }
        execSpec.process {
            // there is no Gulpfile; we know it is going to fail.
            it.ignoreExitValue = true
        }

        final result = nodejs.exec(FORWARD_AND_CAPTURE, FORWARD, execSpec)

        then:
        result.standardOutput.asText.get().contains('Manually set path of gulpfile')
    }

    void 'Run nodejs.exec via a closure'() {
        setup:
        setupGulpDownload(project)
        ExecOutput result
        when:
        project.allprojects {
            // tag::nodeexec-with-closure[]
            result = nodejs.exec(FORWARD_AND_CAPTURE, FORWARD) {
                script {
                    path = 'node_modules/gulp/bin/gulp.js' // <1>
                    args '--help' // <2>
                }
                entrypoint {
                    workingDir(npm.homeDirectory) // <3>
                    executable = nodejs.executable.get() // <4>
                }
                // end::nodeexec-with-closure[]
                process {
                    ignoreExitValue = true
                }
                // tag::nodeexec-with-closure[]
            }
            // end::nodeexec-with-closure[]
        }

        then:
        result.standardOutput.asText.get().contains('Manually set path of gulpfile')
    }

    File setupGulpDownload(Project project) {
        project.allprojects {
            dependencies {
                pnpm npm.pkg(name: 'gulp', tag: ' 5.0.0')
            }
        }
        project.configurations.pnpm.resolve()
        NpmExtension npm = (NpmExtension) (project.extensions.npm)
        new File(npm.homeDirectoryProvider.get(), 'node_modules/gulp/bin/gulp.js')
    }
}