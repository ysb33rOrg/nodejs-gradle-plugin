/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.nodejs.tasks

import org.gradle.testkit.runner.BuildResult
import org.ysb33r.gradle.nodejs.testfixtures.NpmIntegrationTestSpecification

import static org.gradle.testkit.runner.TaskOutcome.SUCCESS

class CmdlineTaskSpec extends NpmIntegrationTestSpecification {

    void setup() {
        buildFile << """
        apply plugin : 'org.ysb33r.nodejs.cmdline'
        
        """.stripIndent()
    }

    void 'Run node from command-line'() {
        when:
        BuildResult result = runTask('node', ['--args=-v --no-warnings', '-i'])

        then:
        result.task(':node').outcome == SUCCESS
        result.output.contains("v${NODEJS_VERSION}")

        when:
        final ccResult = runCcTask('node', ['--args=-v --no-warnings', '-i'])

        then:
        ccResult.task(':node').outcome == SUCCESS
    }

    void 'Run npm from command-line'() {
        when:
        BuildResult result = runTask('npm', ['--args', 'version', '--output=true', '-i'])

        then:
        result.task(':npm').outcome == SUCCESS
        result.output.contains("node: '${NODEJS_VERSION}'")

        when:
        final ccResult = runCcTask('npm', ['--args', 'version'])

        then:
        ccResult.task(':npm').outcome == SUCCESS
    }

    void 'Run npx from command-line'() {
        when:
        BuildResult result = runTask('npx', ['--arg=--help', '--output=true'])

        then:
        result.task(':npx').outcome == SUCCESS
        result.output.contains('Run a command from a local or remote npm package')

        when:
        final ccResult = runCcTask('npx', ['--arg=--help'])

        then:
        ccResult.task(':npx').outcome == SUCCESS
    }

    BuildResult runTask(String taskName, List<String> taskArgs) {
        getGradleRunner(IS_GROOVY_DSL, projectDir, ['-i', '-s', taskName] + taskArgs).build()
    }

    BuildResult runCcTask(String taskName, List<String> taskArgs) {
        getGradleRunnerConfigCache(IS_GROOVY_DSL, projectDir, ['-i', '-s', taskName] + taskArgs).build()
    }
}