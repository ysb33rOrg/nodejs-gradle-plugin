/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.nodejs.tasks

import org.ysb33r.gradle.nodejs.testfixtures.IntegrationSpecification

import static org.gradle.testkit.runner.TaskOutcome.SUCCESS
import static org.ysb33r.gradle.nodejs.plugins.AltDevPlugin.SYNC_TASK

class SyncPackageJsonSpec extends IntegrationSpecification {

    void 'Can sync from dependency list to package.json'() {
        setup:
        writeBuildFile()

        when:
        final result = getGradleRunner(IS_GROOVY_DSL, projectDir, [SYNC_TASK]).build()

        then:
        result.task(":${SYNC_TASK}").outcome == SUCCESS

        when:
        final resultCC = getGradleRunnerConfigCache(IS_GROOVY_DSL, projectDir, [SYNC_TASK, '-s']).build()

        then:
        resultCC.task(":${SYNC_TASK}").outcome == SUCCESS
    }

    void writeBuildFile() {
        buildFile.text = """
        plugins {
            id 'org.ysb33r.nodejs.dev.alt'
        }
        
        nodejs {
            useSystemPath()
        }

        npm {
            homeDirectory = "${buildDir}/test"
        }

        dependencies {
            pnpm npm.pkg(name: 'stringz', tag: '0.2.0')
        }

        """.stripIndent()
    }
}