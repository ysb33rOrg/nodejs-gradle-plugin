/**
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.nodejs;

import org.gradle.api.provider.Provider;
import org.ysb33r.grolifant5.api.core.ConfigCacheSafeOperations;
import org.ysb33r.grolifant5.api.core.ProjectOperations;
import org.ysb33r.grolifant5.api.core.runnable.AbstractCommandExecSpec;

import java.io.File;

/**
 * Base specification for running an NPM command via {@code npm-cli.js}
 *
 * <p> For simplicity Gradle executes {@code npm-cli.js} directly rather
 * than use yet another indirection of the {@code npm} shell script.</p>
 *
 * @since 0.1
 */
public class NpmExecSpec extends AbstractCommandExecSpec<NpmExecSpec> {

    /**
     * Construct class and attach it to specific project.
     *
     * @param nodeLocation      Provider to the location of {@code node} executable.
     * @param npmCliJsLocation  Provider to location of {@code npm-cli.js}.
     * @param ccso Project this exec spec is attached.
     * @since 4.0
     */
    public NpmExecSpec(
            Provider<File> nodeLocation,
            Provider<File> npmCliJsLocation,
            ConfigCacheSafeOperations ccso
    ) {
        super(ccso);

        entrypoint(ep -> {
            ep.executable(nodeLocation);
        });

        runnerSpec(spec -> {
            spec.args(npmCliJsLocation.map(x -> x.getAbsolutePath()));
            spec.args("--scripts-prepend-node-path=true");
        });
    }

    /**
     * Construct class and attach it to specific project.
     *
     * @param nodeLocation      Provider to the location of {@code node} executable.
     * @param npmCliJsLocation  Provider to location of {@code npm-cli.js}.
     * @param projectOperations Project this exec spec is attached.
     * @since 2.0
     *
     * @deprecated Use method taking {@link ConfigCacheSafeOperations} as parameter.
     */
    @Deprecated
    public NpmExecSpec(
            Provider<File> nodeLocation,
            Provider<File> npmCliJsLocation,
            ProjectOperations projectOperations
    ) {
        super(ConfigCacheSafeOperations.from(projectOperations));

        entrypoint(ep -> {
            ep.executable(nodeLocation);
        });

        runnerSpec(spec -> {
            spec.args(npmCliJsLocation.map(x -> x.getAbsolutePath()));
            spec.args("--scripts-prepend-node-path=true");
        });
    }
}
