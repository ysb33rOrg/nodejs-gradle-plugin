/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.nodejs.plugins

import groovy.transform.CompileStatic
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.file.CopySpec
import org.gradle.api.tasks.Delete
import org.gradle.api.tasks.TaskContainer
import org.gradle.api.tasks.bundling.Zip
import org.ysb33r.gradle.nodejs.NpmExtension
import org.ysb33r.gradle.nodejs.tasks.NodeWrappers
import org.ysb33r.gradle.nodejs.tasks.NpmPackageJsonInstall
import org.ysb33r.gradle.nodejs.tasks.SyncProjectToNvmRc
import org.ysb33r.grolifant5.api.core.ProjectOperations
import org.ysb33r.grolifant5.api.core.TaskTools

import static org.ysb33r.gradle.nodejs.plugins.WrapperPlugin.WRAPPER_TASK_NAME

/**
 * Javascript development base plugin.
 *
 * @author Schalk W. Cronjé
 *
 * @since 0.13.0
 */
@CompileStatic
class DevBasePlugin implements Plugin<Project> {
    public static final String DEFAULT_SRCDIR = 'src/node'
    public static final String DEFAULT_GROUP = 'Node.js'
    public static final String ZIP_TASK = 'nodeZip'
    public static final String INSTALL_TASK = 'packageJsonInstall'
    public static final String CLEAN_TASK = 'cleanPackageJsonInstall'
    public static final String SYNC_NVMRC_TASK = 'syncProjectToNvmrc'

    @Override
    void apply(Project project) {
        project.pluginManager.identity {
            apply('base')
            apply(NpmPlugin)
        }

        ProjectOperations po = ProjectOperations.find(project)
        NpmExtension npm = project.extensions.getByType(NpmExtension)
        npm.homeDirectory = DEFAULT_SRCDIR
        npm.localConfig(npm.useLocalUserProfileLocation())
        addPackageJsonTasks(po, project.tasks)
        addZipTask(po, npm)
        addNvmrcSync(po)

        project.pluginManager.withPlugin('org.ysb33r.nodejs.wrapper') {
            po.tasks.named(WRAPPER_TASK_NAME, NodeWrappers) { NodeWrappers wrapper ->
                wrapper.wrapperDestinationDirectory = npm.homeDirectoryProvider
            }
        }
    }

    private void addNvmrcSync(ProjectOperations po) {
        po.tasks.register(SYNC_NVMRC_TASK, SyncProjectToNvmRc) { SyncProjectToNvmRc t ->
            t.group = DEFAULT_GROUP
        }
    }

    private void addZipTask(ProjectOperations po, NpmExtension npm) {
        po.tasks.register(ZIP_TASK, Zip) { zip ->
            zip.identity {
                group = DEFAULT_GROUP
                description = 'Packages Javascript project into ZIP'
                dependsOn(INSTALL_TASK)
                from(npm.homeDirectoryProvider) { CopySpec spec ->
                    spec.include 'app/**'
                    spec.include 'node_modules/**'
                }
            }
        }
    }

    private void addPackageJsonTasks(ProjectOperations po, TaskContainer tasks) {
        final TaskTools taskTools = po.tasks
        taskTools.register(INSTALL_TASK, NpmPackageJsonInstall) { install ->
            install.identity {
                group = DEFAULT_GROUP
                description = 'Installs packages as defined by package.json'
            }
        }

        final nodeModules = taskTools.taskProviderFrom(
            tasks,
            po.providers,
            INSTALL_TASK
        ).map { install ->
            new File(install.extensions.getByType(NpmExtension).homeDirectoryProvider.get(), 'node_modules')
        }

        taskTools.register(CLEAN_TASK, Delete) { del ->
            del.identity {
                group = DEFAULT_GROUP
                description = 'Cleans the node_modules directory'
                delete nodeModules
            }
        }
        taskTools.named('assemble') {
            it.dependsOn(INSTALL_TASK)
        }
    }

}
