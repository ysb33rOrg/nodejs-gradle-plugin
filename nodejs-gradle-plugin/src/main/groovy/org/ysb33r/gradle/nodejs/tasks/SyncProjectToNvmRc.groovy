/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.nodejs.tasks

import groovy.transform.CompileStatic
import org.gradle.api.DefaultTask
import org.gradle.api.provider.Provider
import org.gradle.api.tasks.TaskAction
import org.ysb33r.gradle.nodejs.NodeJSExtension

/**
 * Synchronised the Node version to {@code .nvmrc}.
 *
 * <p>
 *     This task can only execute if there is no synchronisation from {@code .nvmrc} back to the project
 *     and if the version has been explicitly set.
 *     </p>
 *
 *     <p>
 *         The task is explicitly linked at the project-level {@link NodeJSExtension}.
 *         </p>
 * @since 2.0
 */
@CompileStatic
class SyncProjectToNvmRc extends DefaultTask {
    SyncProjectToNvmRc() {
        final nodejs = project.extensions.getByType(NodeJSExtension)

        description = 'Synchronise project Node version into nvmrc'
        this.nodeVersion = nodejs.resolvedExecutableVersion()
        this.nvmrcLocation = nodejs.nvmrcLocation

        final conditionResolver = nodejs.provideResolveNodeByVersion
            .zip(nodejs.provideSyncNodeVersionFromNvmRc) { lhs, rhs -> lhs && !rhs }
        final conditionNvmRcLocation = nodejs.nvmrcLocation
        onlyIf { conditionResolver && conditionNvmRcLocation.present }
    }

    @TaskAction
    void exec() {
        nvmrcLocation.get().text = nodeVersion.get()
    }

    private final Provider<String> nodeVersion
    private final Provider<File> nvmrcLocation
}
