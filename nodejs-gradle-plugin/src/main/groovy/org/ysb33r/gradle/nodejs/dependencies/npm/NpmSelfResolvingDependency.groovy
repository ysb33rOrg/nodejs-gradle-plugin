/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.nodejs.dependencies.npm

import groovy.transform.CompileStatic
import groovy.util.logging.Slf4j
import org.gradle.api.artifacts.FileCollectionDependency
import org.gradle.api.internal.artifacts.dependencies.SelfResolvingDependencyInternal
import org.ysb33r.gradle.nodejs.NodeJSConfigCacheSafeOperations
import org.ysb33r.gradle.nodejs.NodeJSExtension
import org.ysb33r.gradle.nodejs.NpmConfigCacheSafeOperations
import org.ysb33r.gradle.nodejs.NpmDependency
import org.ysb33r.gradle.nodejs.NpmExtension
import org.ysb33r.grolifant5.api.core.ConfigCacheSafeOperations
import org.ysb33r.grolifant5.api.core.ProjectOperations

/**
 * An NPM dependency that can resolve itself.
 *
 * @since 0.5
 */
@CompileStatic
@Slf4j
class NpmSelfResolvingDependency extends BaseNpmSelfResolvingDependency
    implements FileCollectionDependency, SelfResolvingDependencyInternal {

    /**
     * An NPM dependency that can resolve itself.
     *
     * @param projectOperations
     * @param nodejs
     * @param npmExtension
     * @param props Various {@link NpmDependency} properties, but also recognises {@code install-args},
     *   which is a list of additional arguments that can be passed to NPM during package installation.
     *
     * @since 4.0
     */
    NpmSelfResolvingDependency(
        ConfigCacheSafeOperations ccso,
        NodeJSConfigCacheSafeOperations nodejs,
        NpmConfigCacheSafeOperations npm,
        final Map<String, Object> props
    ) {
        super(ccso, nodejs, npm, props)
    }

    /**
     * An NPM dependency that can resolve itself.
     *
     * @param projectOperations
     * @param nodeJSExtension
     * @param npmExtension
     * @param props Various {@link NpmDependency} properties, but also recognises {@code install-args},
     *   which is a list of additional arguments that can be passed to NPM during package installation.
     *
     * @since 0.10
     *
     * @deprecated
     */
    @Deprecated
    NpmSelfResolvingDependency(
        ProjectOperations projectOperations,
        NodeJSExtension nodeJSExtension,
        NpmExtension npmExtension,
        final Map<String, Object> props
    ) {
        super(ConfigCacheSafeOperations.from(projectOperations), nodeJSExtension, npmExtension, props)
    }
}
