/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.nodejs

import groovy.transform.CompileStatic
import org.gradle.api.provider.Property
import org.gradle.api.provider.Provider
import org.gradle.api.provider.SetProperty
import org.ysb33r.gradle.nodejs.tasks.GulpTask
import org.ysb33r.grolifant5.api.core.ConfigCacheSafeOperations

/**
 * Extension that allows for setting of Gulp configuration at a project or task level.
 *
 * @since 0.1
 */
@CompileStatic
class GulpExtension {

    final static String NAME = 'gulp'

    /** Adds the extension to a {@link GulpTask} task.
     *
     * <p> Links the executable resolving to the global instance, but allows
     * it to be overridden on a per-task basis.
     *
     * @param task Task to be extended.
     */
    // tag::example-of-task-overriding1[]
    GulpExtension(GulpTask task) {
        // end::example-of-task-overriding1[]
        this.ccso = ConfigCacheSafeOperations.from(task.project)
        this.gulpFileProvider = task.project.objects.property(File)

        final home = task.project.extensions.getByType(NpmExtension).homeDirectoryProvider
        ccso.fsOperations().updateFileProperty(
            this.gulpFileProvider,
            home.map { new File(it, 'gulpfile.js') }
        )

        // tag::example-of-task-overriding1[]
        this.requires = task.project.objects.setProperty(String) // <.>
//        this.requiresProvider = this.requires.orElse(((GulpExtension) projectExtension).requiresProvider) // <.>
    }
    // end::example-of-task-overriding1[]

    /** Appends more require specifications.
     *
     * @param reqs one of more require specifications
     */
    void requires(String... reqs) {
        requires(reqs as List)
    }

    /** Appends more require specifications.
     *
     * @param reqs Iteratable list of executable requirements
     */
    void requires(final Iterable<String> reqs) {
        if (!this.requires.present) {
            this.requires.value([])
        }

        this.requires.addAll(reqs)
    }

    /** Replace any existing require specifications with a new one.
     *
     * @param reqs Iteratable list of executable requirements.
     */
    void setRequires(final Iterable<String> reqs) {
        this.requires.value([])
        this.requires.addAll(reqs)
    }

    /** Get set of requires that will be passed to Gulp.
     *
     * <p> This is the same as the {@code --requires} command-line.
     *
     * @return An iterable list of require specifications. If the extension is attached to a task and the list is null,
     *   the project extension will be queried. To override the list from the project extension with an empty list,
     *   call {@link #setRequires} with an empty list.
     */
    Iterable<String> getRequires() {
        this.requiresProvider.get()
    }

    /**
     * Lazy-evaluated list of requires.
     *
     * @return A provider to a list of requires.
     *
     * @since 4.0
     */
    Provider<Set<String>> getRequiresProvider() {
        this.requires
    }

    /** Sets the location of {@code gulpfile.js}
     *
     * @param gulpFileLocation
     */
    void setGulpFile(Object gulpFileLocation) {
        ccso.fsOperations().updateFileProperty(this.gulpFileProvider, gulpFileLocation)
    }

    /**
     * The location of {@code gulpfile.js}.
     *
     * @return Returns provider of location. If this is a project extension and the location was not
     *  set {@code project.file( "${project.projectDir}/gulpfile.js"} will be returned.
     *
     * @since 3.0
     */
    Provider<File> getGulpFileProvider() {
        this.gulpFileProvider
    }

    /**
     * The entrypoint path relative to the installed executable folder
     *
     * @return {@code bin/gulp.js}
     */
//    @Override
    protected String getEntryPoint() {
        'bin/gulp.js'
    }

    private final Property<File> gulpFileProvider
    private final Provider<Set<String>> requiresProvider
    private final SetProperty<String> requires
    private final ConfigCacheSafeOperations ccso
}
