/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.nodejs.plugins

import groovy.transform.CompileStatic
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.ysb33r.gradle.nodejs.NodeJSExtension
import org.ysb33r.gradle.nodejs.NpmExtension
import org.ysb33r.gradle.nodejs.dependencies.npm.DependencyHandlerExtension
import org.ysb33r.grolifant5.api.core.ProjectOperations

/**
 *
 * @since 0.1
 */
@CompileStatic
class NpmPlugin implements Plugin<Project> {

    public static final String CONFIGURATION_NAME = 'pnpm'
    public static final String DEPENDENCY_EXTENSION_NAME = 'npm'

    void apply(Project project) {
        project.pluginManager.apply(NodeJSBasePlugin)
        final nodejs = project.extensions.getByType(NodeJSExtension)
        final npm = project.extensions.create(NpmExtension.NAME, NpmExtension, project)
        addNpmPackageExtension(project, nodejs, npm)
        addConfigurations(project)
    }

    void addConfigurations(Project project) {
        project.configurations.create(CONFIGURATION_NAME)
    }

    void addNpmPackageExtension(Project project, NodeJSExtension nodeJSExtension, NpmExtension npm) {
        project.dependencies.extensions.create(
            DEPENDENCY_EXTENSION_NAME,
            DependencyHandlerExtension,
            ProjectOperations.find(project),
            nodeJSExtension,
            npm
        )
    }
}
