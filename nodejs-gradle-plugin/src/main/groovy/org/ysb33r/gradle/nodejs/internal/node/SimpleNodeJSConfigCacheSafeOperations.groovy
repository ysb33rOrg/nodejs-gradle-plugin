/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.nodejs.internal.node

import groovy.transform.CompileStatic
import org.gradle.api.provider.Provider
import org.ysb33r.gradle.nodejs.NodeJSConfigCacheSafeOperations

/**
 * Store simple values for executing {@code node} in a configuration cache-safe manner.
 *
 * @author Schalk W. Cronjé
 * @since 4.0
 */
@CompileStatic
class SimpleNodeJSConfigCacheSafeOperations implements NodeJSConfigCacheSafeOperations {
    final Provider<File> executable
    final Provider<File> npmCliJsProvider
    final Provider<Map<String, String>> environmentProvider

    SimpleNodeJSConfigCacheSafeOperations(NodeJSConfigCacheSafeOperations other) {
        this.executable = other.executable
        this.npmCliJsProvider = other.npmCliJsProvider
        this.environmentProvider = other.environmentProvider
    }

    SimpleNodeJSConfigCacheSafeOperations(
        final Provider<File> executable,
        final Provider<File> npmCliJsProvider,
        final Provider<Map<String, String>> environmentProvider
    ) {
        this.executable = executable
        this.npmCliJsProvider = npmCliJsProvider
        this.environmentProvider = environmentProvider
    }
}
