/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.nodejs.tasks

import groovy.transform.CompileStatic
import org.apache.commons.text.StringTokenizer
import org.apache.commons.text.matcher.StringMatcherFactory
import org.gradle.api.logging.LogLevel
import org.gradle.api.provider.MapProperty
import org.gradle.api.provider.Property
import org.gradle.api.provider.Provider
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.Internal
import org.gradle.api.tasks.TaskAction
import org.gradle.api.tasks.options.Option
import org.ysb33r.gradle.nodejs.NodeJSExtension
import org.ysb33r.gradle.nodejs.NpmExtension
import org.ysb33r.grolifant5.api.core.runnable.GrolifantDefaultTask

import static org.ysb33r.gradle.nodejs.plugins.DevBasePlugin.DEFAULT_GROUP
import static org.ysb33r.grolifant5.api.core.ExecTools.OutputType.CAPTURE
import static org.ysb33r.grolifant5.api.core.ExecTools.OutputType.FORWARD

/**
 * A base class for creating command-line tasks.
 *
 * @author Schalk W. Cronjé
 */
@CompileStatic
class AbstractCmdlineTask extends GrolifantDefaultTask {

    /**
     * By default, this task is never up to date. Setting this value to {@code true} gives it the option to be
     * up to date.
     */
    @Internal
    boolean canBeUpToDate = false

    @Option(option = 'arg', description = 'Command-line option (Can be repeated)')
    void setArgs(List<Object> args) {
        this.args.addAll(args)
    }

    @Option(option = 'args', description = 'Command-line options (space separated). Applied before single arguments.')
    void setArgsLine(String args) {
        StringTokenizer st = new StringTokenizer(args)
        st.quoteMatcher = StringMatcherFactory.INSTANCE.quoteMatcher()
        this.args.addAll(st.tokenList)
    }

    /**
     * Ability to turn output on or off.
     *
     * <p>
     * By default output is displayed when logging level is {@code INFO}.
     * Error output is always displayed if there is an error.
     * </p>
     * @param flag {@code true} to turn output on, or {@code false} to turn it off.
     *
     * @since 4.0
     */
    @Option(option = 'output', description = 'Turn output on or off')
    void setConsoleOutput(String flag) {
        this.showConsoleOutput.set(flag.toBoolean())
    }

    @Input
    List<String> getFinalArgs() {
        stringTools().stringize(this.args)
    }

    void setCommandProvider(Object cmdProvider) {
        stringTools().updateStringProperty(this.cmdProvider, cmdProvider)
    }

    void setEnvironmentProvider(Provider<Map<String, String>> env) {
        this.envProvider.set(env)
    }

    void setWorkingDir(Object dir) {
        fsOperations().updateFileProperty(this.workingDirProvider, dir)
    }

    @TaskAction
    void exec() {
        final boolean console = showConsoleOutput.present ? showConsoleOutput.get() : null
        final cmd = this.cmdProvider.get()
        final env = this.envProvider.get()
        final wd = this.workingDirProvider.get()

        final withOutput = (verbose && console != false) || notQuiet && (console == null || console == true)
        final mode = withOutput ? FORWARD : CAPTURE
        final result = execTools().exec(mode, mode, execSpec -> {
            execSpec.environment = env
            execSpec.executable = cmd
            execSpec.args = finalArgs
            execSpec.workingDir = wd
            execSpec.ignoreExitValue = true
        })

        final exitValue = result.result.get().exitValue

        if (exitValue) {
            if (!verbose) {
                logger.error(result.standardError.asText.get())
            }
            result.assertNormalExitValue()
        }
    }

    protected AbstractCmdlineTask() {
        extensions.create(NpmExtension.NAME, NpmExtension, this)
        extensions.create(NodeJSExtension.NAME, NodeJSExtension, this)

        this.cmdProvider = project.objects.property(String)
        this.workingDirProvider = project.objects.property(File)
        this.showConsoleOutput = project.objects.property(Boolean)
        this.verbose = project.gradle.startParameter.logLevel in [LogLevel.INFO, LogLevel.DEBUG]
        this.notQuiet = project.gradle.startParameter.logLevel != LogLevel.QUIET
        this.commandProvider = nodejs.executable.map { it.absolutePath }
        this.envProvider = project.objects.mapProperty(String, String)
        this.envProvider.set(nodejs.environmentProvider)

        group = DEFAULT_GROUP
        workingDir = npm.homeDirectoryProvider

        outputs.upToDateWhen { AbstractCmdlineTask task -> task.canBeUpToDate }
    }

    @Internal
    protected NpmExtension getNpm() {
        extensions.getByType(NpmExtension)
    }

    @Internal
    protected NodeJSExtension getNodejs() {
        extensions.getByType(NodeJSExtension)
    }

    /**
     * Allows the derived task to push additional arguments that will usually precede any arguments derived from
     * command-line.
     *
     * @param args One or more args.
     */
    protected void addArgs(Object... args) {
        this.args.addAll(args)
    }

    private final Property<String> cmdProvider
    private final Property<File> workingDirProvider
    private final List<Object> args = []
    private final MapProperty<String, String> envProvider
    private final Property<Boolean> showConsoleOutput
    private final boolean verbose
    private final boolean notQuiet
}
