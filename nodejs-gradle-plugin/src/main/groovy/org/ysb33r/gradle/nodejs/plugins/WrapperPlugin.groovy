/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.nodejs.plugins

import groovy.transform.CompileDynamic
import groovy.transform.CompileStatic
import org.gradle.api.Action
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.Task
import org.gradle.api.XmlProvider
import org.gradle.api.tasks.TaskProvider
import org.gradle.plugins.ide.idea.model.IdeaModel
import org.ysb33r.gradle.nodejs.NodeJSExtension
import org.ysb33r.gradle.nodejs.NpmExtension
import org.ysb33r.gradle.nodejs.tasks.NodeBinariesCacheTask
import org.ysb33r.gradle.nodejs.tasks.NodeWrappers

/** Provides a task for generating wrappers for {@code node}, {@code npx}
 * and {@code npm}.
 *
 * @since 0.9.0
 */
@CompileStatic
class WrapperPlugin implements Plugin<Project> {
    public static final String CACHE_TASK_NAME = 'cacheNodeBinaries'
    public static final String WRAPPER_TASK_NAME = 'nodeWrappers'

    @Override
    void apply(Project project) {
        project.apply plugin: NpmPlugin

        final npm = project.extensions.getByType(NpmExtension)
        final nodejs = project.extensions.getByType(NodeJSExtension)
        final cacheTask = project.tasks.register(CACHE_TASK_NAME, NodeBinariesCacheTask)
        final nodeWrapper = project.tasks.register(WRAPPER_TASK_NAME, NodeWrappers)

        cacheTask.configure {
            it.group = DevBasePlugin.DEFAULT_GROUP
            it.description = 'Cache node binaries'
        }

        nodeWrapper.configure {
            it.group = DevBasePlugin.DEFAULT_GROUP
            it.description = 'Generated wrappers for node, npm & npx'
            it.associateCacheTask(cacheTask)
        }

        configureForIdea(project, nodeWrapper, nodejs, npm)
    }

    private void configureForIdea(
        Project project,
        TaskProvider<NodeWrappers> nodeWrapper,
        NodeJSExtension nodeJSExtension,
        NpmExtension npm
    ) {
        project.pluginManager.withPlugin('idea') {
            final cacheTask = project.tasks.register('ideaWorkspaceCacheNode') { Task t ->
                t.doLast {
                    t.logger.info "Cached ${nodeJSExtension.executable.get()}"
                }
            }
            project.tasks.named('ideaWorkspace').configure { Task t ->
                t.dependsOn(cacheTask)
            }
            project.tasks.named('ideaModule').configure { Task t ->
                t.dependsOn(cacheTask)
            }
            project.tasks.named('ideaProject').configure { Task t ->
                t.dependsOn(cacheTask)
            }
            project.extensions.getByType(IdeaModel).workspace.iws.withXml(new Action<XmlProvider>() {
                @Override
                void execute(XmlProvider xml) {
                    def component = maybeCreatePropertiesComponent(xml.asNode())

                    setPropertyValue(
                        component,
                        'nodejs_interpreter_path',
                        "${nodeWrapper.get().nodeWrapperProvider.get().absolutePath}"
                    )

                    setPropertyValue(
                        component,
                        'nodejs_package_manager_path',
                        "${npm.executable.get().parentFile.parentFile.absolutePath}"
                    )

                    setPropertyValue(
                        component,
                        'settings.editor.selected.configurable',
                        'settings.nodejs'
                    )
                }
            })
        }
    }

    @CompileDynamic
    private Node maybeCreatePropertiesComponent(Node root) {
        def component = root.component.find { it.@name == COMPONENT }

        if (component == null) {
            root.appendNode('component', [id: COMPONENT])
        } else {
            component
        }
    }

    @CompileDynamic
    private void setPropertyValue(Node component, String attrName, Object value) {
        def node = findPropertyByName(component, attrName)
        if (node == null) {
            component.appendNode('property', [
                name : attrName,
                value: value
            ])
        } else {
            node.attributes()[attrName] = value
        }
    }

    @CompileDynamic
    private Node findPropertyByName(Node component, String propName) {
        component.property.find { it.@name == propName }
    }

    private static final String COMPONENT = 'PropertiesComponent'
}
