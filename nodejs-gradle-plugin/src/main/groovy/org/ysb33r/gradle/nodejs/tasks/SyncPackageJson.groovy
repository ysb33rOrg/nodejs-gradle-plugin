/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.nodejs.tasks

import groovy.transform.CompileStatic
import org.gradle.api.artifacts.DependencySet
import org.gradle.api.provider.ListProperty
import org.gradle.api.provider.Property
import org.gradle.api.provider.Provider
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.Internal
import org.gradle.api.tasks.OutputFile
import org.gradle.api.tasks.TaskAction
import org.ysb33r.gradle.nodejs.NpmDependencyGroup
import org.ysb33r.gradle.nodejs.NpmExtension
import org.ysb33r.gradle.nodejs.PackageJson
import org.ysb33r.gradle.nodejs.dependencies.npm.NpmSelfResolvingDependency
import org.ysb33r.grolifant5.api.core.ProjectOperations
import org.ysb33r.grolifant5.api.core.runnable.GrolifantDefaultTask

import java.util.concurrent.Callable

import static java.util.Collections.EMPTY_LIST
import static org.ysb33r.gradle.nodejs.NpmDependencyGroup.DEVELOPMENT
import static org.ysb33r.gradle.nodejs.NpmDependencyGroup.OPTIONAL
import static org.ysb33r.gradle.nodejs.NpmDependencyGroup.PRODUCTION
import static org.ysb33r.gradle.nodejs.PackageJson.Indentation.FOUR_SPACES
import static org.ysb33r.gradle.nodejs.PackageJson.Indentation.TWO_SPACES
import static org.ysb33r.gradle.nodejs.PackageJson.PACKAGE_JSON
import static org.ysb33r.gradle.nodejs.PackageJson.formatName
import static org.ysb33r.gradle.nodejs.plugins.DevBasePlugin.DEFAULT_GROUP

/** Task that will sync various project settings int a {@code package.json} file.
 *
 * @since 0.13.0
 */
@CompileStatic
class SyncPackageJson extends GrolifantDefaultTask {

    SyncPackageJson() {
        group = DEFAULT_GROUP
        description = 'Updates version inside package.json'

        this.configurationProvider = project.objects.listProperty(DependencySet)
        this.configurationProvider.value(EMPTY_LIST)

        final npm = project.extensions.getByType(NpmExtension)
        this.packageJson = project.objects.property(File)
        this.packageJson.set(npm.homeDirectoryProvider.map { new File(it, PACKAGE_JSON) })

        this.packageJsonVersion = project.objects.property(String)
        this.packageJsonVersion.set(projectTools().versionProvider.orElse('0.0.0'))

        this.packageJsonName = project.objects.property(String)
        this.packageJsonName.set(formatName(project.name))
    }

    @Input
    String getProjectName() {
        this.packageJsonName
    }

    @Input
    Provider<String> getVersionProvider() {
        this.packageJsonVersion
    }

    /** Force NPM semantic versioning to be used.
     *
     * If project.version is {@code 1.0} then {@code 1.0.0} will used as the version for {@code package.json}.
     * Like if the version is {@code 1.0-alpha1} then {@code 1.0.0-alpha1} will be used.
     *
     * Semantic versioning is forced by default.
     */
    @Input
    boolean forceSemver = true

    /** Whether to force a two-space indent when rewriting the file.
     *
     */
    @Internal
    boolean forceTwoSpaceIndent = false

    /** Sorts the output keys.
     *
     * Mimics the functionality of https://www.npmjs.com/package/sort-package-json. Default is to not sort.
     */
    @Internal
    boolean sortOutput = false

    /** Set the name for {@code package.json} file. If not set it will default
     * to a NPM-safe {@code project.name}.
     *
     * @param projName Anything convertible to a string using
     * {@link org.ysb33r.grolifant5.api.core.StringTools#stringize}.
     *
     * @since 0.9.0
     */
    void setPackageJsonName(Object projName) {
        stringTools().updateStringProperty(this.packageJsonName, { ->
            formatName(stringTools().stringize(projName))
        } as Callable<String>)
    }

    /** Project name that will be use for {@code package.json}
     *
     * @return Version string
     *
     * @since 0.9.0
     */
    @Input
    Provider<String> getPackageJsonName() {
        this.packageJsonName
    }

    /** Set the location of the {@code package.json} file
     *
     * @param file Anything convertible to a {@link java.io.File} using
     * {@link org.ysb33r.grolifant5.api.core.FileSystemOperations#file}.
     */
    void setPackageJsonFile(Object file) {
        fsOperations().updateFileProperty(this.packageJson, file)
    }

    /** Location of the {@code package.json} file.
     *
     * @return Provider to the location of {@code package.json}
     */
    @OutputFile
    Provider<File> getPackageJsonFile() {
        this.packageJson
    }

    /**
     * Adds configurations which contain NPM dependencies.
     *
     * These dependencies will added to the correct scope in the {@code package.json} file.
     *
     * @param configs Configurations instances or anything that can be resolved to a configuration name.
     */
    void npmConfigurations(Object... configs) {
        configurationProvider.addAll(configs.collect {
            ProjectOperations.find(project).configurations.asConfiguration(it)
        }*.allDependencies)
    }

    @TaskAction
    void exec() {
        final configs = configurationProvider.get()
        final packageFile = packageJsonFile.get()
        final output = PackageJson.of(packageJsonVersion.get(), packageJsonName.get())
        List<NpmSelfResolvingDependency> deps = configs.empty ? EMPTY_LIST : findNpmDependencies(configs)

        if (!deps.empty) {
            output.dependencies.putAll(findDeps(deps, PRODUCTION))
            output.devDependencies.putAll(findDeps(deps, DEVELOPMENT))
            output.optionalDependencies.putAll(findDeps(deps, OPTIONAL))
        }

        output.writeToFile(packageFile, forceTwoSpaceIndent ? TWO_SPACES : FOUR_SPACES, sortOutput)
    }

    private Map<String, String> findDeps(List<NpmSelfResolvingDependency> deps, NpmDependencyGroup group) {
        deps.findAll {
            it.installGroup == group
        }.collectEntries {
            String key = it.scope ? "@${it.scope}/${it.name}" : it.name
            [key, it.version]
        }
    }

    private List<NpmSelfResolvingDependency> findNpmDependencies(List<DependencySet> depSets) {
        final List<NpmSelfResolvingDependency> deps = []
        depSets.forEach { depSet ->
            final nrdSet = depSet.findAll {
                it instanceof NpmSelfResolvingDependency
            } as Set<NpmSelfResolvingDependency>
            deps.addAll(nrdSet)
        }
        deps
    }

    private final Property<String> packageJsonVersion
    private final Property<String> packageJsonName
    private final Property<File> packageJson
    private final ListProperty<DependencySet> configurationProvider
}
