/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.nodejs.dependencies.npm

import groovy.transform.CompileStatic
import org.ysb33r.gradle.nodejs.NodeJSExtension
import org.ysb33r.gradle.nodejs.NpmExtension
import org.ysb33r.grolifant5.api.core.ConfigCacheSafeOperations
import org.ysb33r.grolifant5.api.core.ProjectOperations

/**
 * Adds a {@code npm} extension to a dependency handler.
 *
 * @since 2.0
 */
@CompileStatic
class DependencyHandlerExtension {

    DependencyHandlerExtension(ProjectOperations po, NodeJSExtension nodejs, NpmExtension npm) {
        this.projectOperations = po
        this.nodeJSExtension = nodejs
        this.npmExtension = npm
    }

    /**
     * Creates a NPM dependency from a specification.
     * <p>
     *     The following keys are allowed:
     *     <ul>
     *         <li>scope<li>
     *         <li>name</li>
     *         <li>tag</li>>
     *         <li>type</li>
     *         </ul>
     *     </p>
     *
     * @param spec Specification as a map
     * @return NPM dependency.
     */
    NpmSelfResolvingDependency pkg(Map<String, ?> spec) {
        new NpmSelfResolvingDependency(
            ConfigCacheSafeOperations.from(projectOperations),
            nodeJSExtension,
            npmExtension,
            spec
        )
    }

    private final ProjectOperations projectOperations
    private final NodeJSExtension nodeJSExtension
    private final NpmExtension npmExtension
}
