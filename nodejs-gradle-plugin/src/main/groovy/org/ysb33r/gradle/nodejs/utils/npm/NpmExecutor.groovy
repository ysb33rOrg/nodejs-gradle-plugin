/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.nodejs.utils.npm

import groovy.transform.CompileStatic
import groovy.util.logging.Slf4j
import org.gradle.api.GradleException
import org.gradle.api.file.FileTree
import org.gradle.api.provider.Provider
import org.gradle.process.ExecSpec
import org.ysb33r.gradle.nodejs.NodeJSConfigCacheSafeOperations
import org.ysb33r.gradle.nodejs.NodeJSExtension
import org.ysb33r.gradle.nodejs.NodeJSExtensionMethods
import org.ysb33r.gradle.nodejs.NpmConfigCacheSafeOperations
import org.ysb33r.gradle.nodejs.NpmDependencyGroup
import org.ysb33r.gradle.nodejs.NpmExecSpec
import org.ysb33r.gradle.nodejs.NpmExtension
import org.ysb33r.gradle.nodejs.NpmExtensionMethods
import org.ysb33r.gradle.nodejs.NpmPackageDescriptor
import org.ysb33r.gradle.nodejs.PackageJson
import org.ysb33r.gradle.nodejs.internal.Transform
import org.ysb33r.grolifant5.api.core.ConfigCacheSafeOperations
import org.ysb33r.grolifant5.api.core.ExecTools
import org.ysb33r.grolifant5.api.core.OperatingSystem
import org.ysb33r.grolifant5.api.core.ProjectOperations
import org.ysb33r.grolifant5.api.core.runnable.ExecOutput

import static org.ysb33r.gradle.nodejs.PackageJson.PACKAGE_JSON
import static org.ysb33r.grolifant5.api.core.ExecTools.OutputType.CAPTURE
import static org.ysb33r.grolifant5.api.core.ExecTools.OutputType.FORWARD
import static org.ysb33r.grolifant5.api.core.ExecTools.OutputType.QUIET

/**
 * Utility methods that aids in running NPM.
 *
 * @since 0.5
 */
@CompileStatic
@Slf4j
class NpmExecutor {

    public static final String NPM_INSTALL = 'install'
    public static final String OMIT_DEV = '--omit=dev'

    /**
     * Use this value to force the use of the bundled NPM rather than the version supplied by an NPM extension.
     * This is used with {@link #createNpmExecSpec} and in the constructor
     */
    public static final boolean FORCE_NODE_DEFAULT_FOR_NPM = true

    /**
     * Constructs an object that can be used to execute {@code npm}.
     *
     * @param configCacheSafeOperations Operations for the specific project context that are config cache-safe.
     * @param nodeJS {@link NodeJSExtension} to use for resolving {@code node} location.
     * @param npm {@link NpmExtension} to use for resolving NPM-related activities.
     * @param forceNodeDefaultForNpm Whether to force the NPM to be used from the bundled NPM rather than
     *   the one supplied by the NPM extension. Default is to use the version supplied by the NPM extension.
     *
     * @since 4.0
     */
    NpmExecutor(
        ConfigCacheSafeOperations configCacheSafeOperations,
        NodeJSConfigCacheSafeOperations nodeJS,
        NpmConfigCacheSafeOperations npm,
        boolean forceNodeDefaultForNpm = false
    ) {
        this.ccso = configCacheSafeOperations.from(configCacheSafeOperations)
        this.nodeJS = NodeJSConfigCacheSafeOperations.from(nodeJS)
        this.npm = NpmConfigCacheSafeOperations.from(npm)
        this.forceNodeDefaultForNpm = forceNodeDefaultForNpm
    }

    /**
     * Constructs an object that can be used to execute {@code npm}.
     *
     * @param configCacheSafeOperations Operations for the specific project context that are config cache-safe.
     * @param nodeJS {@link NodeJSExtension} to use for resolving {@code node} location.
     * @param npm {@link NpmExtension} to use for resolving NPM-related activities.
     * @param forceNodeDefaultForNpm Whether to force the NPM to be used from the bundled NPM rather than
     *   the one supplied by the NPM extension. Default is to use the version supplied by the NPM extension.
     *
     * @since 4.0
     */
    NpmExecutor(
        ConfigCacheSafeOperations configCacheSafeOperations,
        NodeJSExtensionMethods nodeJS,
        NpmExtensionMethods npm,
        boolean forceNodeDefaultForNpm = false
    ) {
        this.ccso = configCacheSafeOperations.from(configCacheSafeOperations)
        this.nodeJS = NodeJSConfigCacheSafeOperations.from(nodeJS)
        this.npm = NpmConfigCacheSafeOperations.from(npm)
        this.forceNodeDefaultForNpm = forceNodeDefaultForNpm
    }

    /**
     *
     * @param projectOperations Operations for the specific project context
     * @param nodeJS {@link NodeJSExtension} to use for resolving {@code node} location.
     * @param npm {@link NpmExtension} to use for resolving NPM-related activities.
     * @param forceNodeDefaultForNpm Whether to force the NPM to be used from the bundled NPM rather than
     *   the one supplied by the NPM extension. Default is to use the version supplied by the NPM extension.
     *
     * @since 0.10
     *
     * @deprecated Use constructor that takes a {@link ConfigCacheSafeOperations} instance instead.
     */
    @Deprecated
    NpmExecutor(
        ProjectOperations projectOperations,
        NodeJSExtensionMethods nodeJS,
        NpmExtensionMethods npm,
        boolean forceNodeDefaultForNpm = false
    ) {
        this.ccso = ConfigCacheSafeOperations.from(projectOperations)
        this.nodeJS = NodeJSConfigCacheSafeOperations.from(nodeJS)
        this.npm = NpmConfigCacheSafeOperations.from(npm)
        this.forceNodeDefaultForNpm = forceNodeDefaultForNpm
    }

    /**
     * Runs NPM given a fully-configured execution specification.
     *
     * @param stdout What to do with standard output.
     * @param stderr What to do with error output.
     * @param es Specification to execute.
     * @return Execution result.
     * @throw May throw depending on whether execution specification was mal-configured or whether
     *   execution itself failed.
     *
     * @since 4.0
     */
    ExecOutput runNpm(ExecTools.OutputType stdout, ExecTools.OutputType stderr, NpmExecSpec es) {
        es.entrypoint {
            log.info "NPM will execute ${executable} using working directory ${workingDir}"
            log.debug "NPM environment: ${environment}"
        }
        this.ccso.execTools().exec(stdout, stderr) { ExecSpec spec ->
            es.copyTo(spec)
        }
    }

    /**
     * Installs an NPM executable by running {@code npmExt install} in a controlled environment.
     *
     * @param npmPackageDescriptor Description of NPM executable.
     * @param installGroup Production, development of optional installation.
     * @param additionalArgs Any additional arguments that might be deemed necessary to customise the installation.
     *   This is here to provide flexibility as the author cannot foresee all common use cases.
     *   Should rarely be used. Should you find using this method regular occurrence it might be prudent to
     *   to raise an issue to ask to a feature update and explaining the context in which this is needed.
     * @param additionalEnvironment Additional environmental settings when installing. This can be used to set the
     *   system path variable if need be.
     * @return List of files that were installed
     *
     * @sa https://docs.npmjs.com/cli/install
     *
     * @since 0.10
     */
    FileTree installNpmPackage(
        final NpmPackageDescriptor npmPackageDescriptor,
        final NpmDependencyGroup installGroup,
        Iterable<String> additionalArgs,
        Map<String, Object> additionalEnvironment = [:]
    ) {
        Map<String, Object> env = [:]
        env.putAll(additionalEnvironment)
        updateEnvPath(env)
        final pathVar = OS.pathVar

        File wd
        NpmExecSpec execSpec = createNpmExecSpecWithDefaultEnvironment(
            this.ccso,
            nodeJS,
            npm,
            forceNodeDefaultForNpm
        )

        execSpec.entrypoint {
            final defaultPath = environment[pathVar]
            if (defaultPath) {
                env[pathVar] = "${defaultPath}${OS.pathSeparator}${env[pathVar]}"
            }
            environment(env)
            wd = workingDir
        }

        execSpec.cmd {
            command = NPM_INSTALL
            args npmPackageDescriptor.npmPackageCoordinates, installGroup.saveArgument
            args additionalArgs
        }

        runNpm(FORWARD, FORWARD, execSpec).assertNormalExitValue()

        String scope = npmPackageDescriptor.scope ? "@${npmPackageDescriptor.scope}/" : ''
        this.ccso.fsOperations().fileTree("${wd}/node_modules/${scope}${npmPackageDescriptor.packageName}")
    }

    /**
     * Create a NPM package installer that is configuration cache safe.
     *
     * @param packageJson Location of {@code package.json} file.
     *   This will affect the NPM home directory.
     * @param additionalArgs Additional arguments to be passed to {@code npm install}
     * @param productionOnly Whether this is production only.
     * @return An installer.
     *
     * @since 5.0
     */
    NpmPackageInstaller createNpmPackageInstaller(
        Provider<File> packageJson,
        Provider<List<String>> additionalArgs,
        Provider<Boolean> productionOnly
    ) {
        NpmExecSpec execSpec = createNpmExecSpecWithDefaultEnvironment(this.ccso, nodeJS, npm)
        final homeDir = packageJson.map { it.parentFile }
        final installArgs = productionOnly.zip(additionalArgs) { prod, args ->
            if (prod) {
                [OMIT_DEV] + args
            } else {
                args
            }
        }

        execSpec.cmd {
            command = NPM_INSTALL
            addCommandLineArgumentProviders(installArgs)
        }

        execSpec.process {
            ignoreExitValue = true
        }

        new NpmPackageInstaller() {
            @Override
            FileTree run() {
                final home = homeDir.get()
                final pj = packageJson.get()
                final root = new File(home, 'node_modules').absolutePath

                if (pj.name != PACKAGE_JSON || !pj.exists()) {
                    throw new GradleException("${pj} does not exist or is not a valid description file")
                }
                if (pj.parentFile != home) {
                    throw new GradleException("${pj} is not a child of ${home}")
                }

                // Need to set working dir here, as it is otherwise evaluates prematurely
                execSpec.entrypoint {
                    workingDir(homeDir)
                }

                final result = runNpm(CAPTURE, CAPTURE, execSpec)
                if (result.result.get().exitValue) {
                    log.error(result.standardError.asText.get())
                    log.info(result.standardOutput.asText.get())
                }
                result.assertNormalExitValue()

                final descriptor = PackageJson.parsePackageJson(pj)
                Set<String> pkgNames = []
                pkgNames.addAll(descriptor.dependencies.keySet())
                pkgNames.addAll(descriptor.devDependencies.keySet())
                pkgNames.addAll(descriptor.optionalDependencies.keySet())
                if (pkgNames.empty) {
                    NpmExecutor.this.ccso.fsOperations().emptyFileCollection().asFileTree
                }

                Set<String> pkgDirectories = Transform.toSet(pkgNames) { String name ->
                    "${root}/${name}".toString()
                }

                final dirsToScan = NpmExecutor.this.ccso.fsOperations().files(pkgDirectories)
                FileTree tree = NpmExecutor.this.ccso.fsOperations().fileTree(dirsToScan)
                pkgDirectories.forEach { dir ->
                    File nextPackageJson = new File(root, PACKAGE_JSON)
                    if (nextPackageJson.exists()) {
                        log.debug "Processing '${nextPackageJson}' for NPM dependencies"
                        FileTree nextCollection = calculateInstallableFiles(nextPackageJson)
                        if (nextCollection != null) {
                            tree += nextCollection
                        }
                    }
                }
                tree
            }
        }
    }

    /**
     * Create a NPM package installer that is configuration cache safe and which will install all packages
     * including dev.
     *
     * @param packageJson Location of {@code package.json} file.
     * @param additionalArgs Additional arguments to be passed to {@code npm install}
     * @return An installer.
     *
     * @since 5.0
     */
    NpmPackageInstaller createNpmPackageInstaller(
        Provider<File> packageJson,
        Provider<List<String>> additionalArgs
    ) {
        createNpmPackageInstaller(
            packageJson,
            additionalArgs,
            this.ccso.providerTools().provider { -> false }
        )
    }

    /**
     * Installs packages from a {@code package.json} description.
     *
     * @param packageJson Path to a {@code package.json} file.
     * @param additionalArgs Additional arguments to be passed to NPM install command.
     * @param productionOnly Only install the production packages.
     * @return {@link org.gradle.api.file.FileTree} with lists of files that were installed.
     *   Will also include files from packages that were already there, but which would have been installed otherwise.
     * @throw GradleException if {@code packageJson} does not exist or is not in the
     * {@code npmExtension.homeDirectoryProperty}.
     *
     * @since 0.10
     */
    FileTree installPackagesFromDescription(
        final File packageJson,
        Iterable<String> additionalArgs,
        boolean productionOnly = false
    ) {
        if (packageJson.name != PACKAGE_JSON || !packageJson.exists()) {
            throw new GradleException("${packageJson} does not exist or is not a valid description file")
        }

        final homeDir = npm.homeDirectoryProvider.get()
        if (packageJson.parentFile != homeDir) {
            throw new GradleException("${packageJson} is not a child of ${homeDir}")
        }

        NpmExecSpec execSpec = createNpmExecSpecWithDefaultEnvironment(this.ccso, nodeJS, npm)
        execSpec.cmd {
            command = NPM_INSTALL
            if (productionOnly) {
                args(OMIT_DEV)
                args(packageJson.parentFile.absolutePath)
                args(additionalArgs)
            }
        }
        execSpec.process {
            ignoreExitValue = true
        }

        final result = runNpm(CAPTURE, CAPTURE, execSpec)
        if (result.result.get().exitValue) {
            log.error(result.standardError.asText.get())
            log.info(result.standardOutput.asText.get())
        }
        result.assertNormalExitValue()
        calculateInstallableFiles(packageJson)
    }

    /** Works out where the installation folder will be for a executable.
     *
     * @param npmPackageDescriptor Description of NPM executable
     * @return The path where the executable will be installed to
     *
     * @since 0.10
     */
    File getPackageInstallationFolder(
        final NpmPackageDescriptor npmPackageDescriptor
    ) {
        NpmExecSpec execSpec = createNpmExecSpecWithDefaultEnvironment(this.ccso, nodeJS, npm)
        String scope = npmPackageDescriptor.scope ? "@${npmPackageDescriptor.scope}/" : ''
        File wd
        execSpec.entrypoint {
            wd = workingDir
        }
        new File("${wd}/node_modules/${scope}${npmPackageDescriptor.packageName}")
    }

    /**
     * Creates a template {@code package.json} file.
     *
     * @param projectName Name of the project.
     * @param projectVersion Version/tag of the project.
     * @return Location of the generated file.
     *
     * @since 0.10
     */
    File initPkgJson(
        final String projectName,
        final Provider<String> projectVersion
    ) {
        final String name = "\"name\": \"${projectName}\","
        final String version = "\"version\": \"${projectVersion.get()}\","
        final String description = "\"description\": \"${projectName}\","

        NpmExecSpec execSpec = createNpmExecSpecWithDefaultEnvironment(this.ccso, nodeJS, npm)
        File wd
        execSpec.entrypoint {
            wd = workingDir
        }
        execSpec.cmd {
            command = 'init'
            args '-f', '-s'
        }
        runNpm(QUIET, QUIET, execSpec).assertNormalExitValue()

        File packageJson = new File(wd, PACKAGE_JSON)

        if (!packageJson.exists()) {
            throw new GradleException("${packageJson.absolutePath} was not created as expected")
        }

        packageJson.text = packageJson.text.
            replaceAll(~/"name":\s+".+?",/, name).
            replaceAll(~/"version":\s+".+?",/, version).
            replaceAll(~/"description":\s+".+?",/, description)

        packageJson
    }

    /**
     * Returns a live set of installable files.
     *
     * <p> This is an approximation. It parses the {@code package.json} file to discover dependencies, then
     *   recursively parses all other {@code package.json} files it find in those dependencies.
     *
     * @param rootPackageJson Initial package.json file to start traversal.
     * @return Live file collection, meaning it is possible to add more executable directories.
     *   Returns null if no dependencies, optional dependencies or dev dependencies were found.
     * @throw GradleException if {@code packageJson} does not exist or is not in the
     * {@code npmExtension.homeDirectoryProperty}.
     *
     * @since 0.10
     */
    FileTree calculateInstallableFiles(File rootPackageJson) {
        if (rootPackageJson.name != PACKAGE_JSON || !rootPackageJson.exists()) {
            throw new GradleException("${rootPackageJson} does not exist or is not a valid description file")
        }

        final homeDir = npm.homeDirectoryProvider.get()
        if (rootPackageJson.parentFile != homeDir) {
            throw new GradleException("${rootPackageJson} is not a child of ${homeDir}")
        }

        PackageJson descriptor = PackageJson.parsePackageJson(rootPackageJson)
        Set<String> pkgNames = []
        pkgNames.addAll(descriptor.dependencies.keySet())
        pkgNames.addAll(descriptor.devDependencies.keySet())
        pkgNames.addAll(descriptor.optionalDependencies.keySet())
        if (pkgNames.empty) {
            return null
        }

        String root = new File(homeDir, 'node_modules').absolutePath
        Set<String> pkgDirectories = Transform.toSet(pkgNames) { String name ->
            "${root}/${name}".toString()
        }

        FileTree tree = this.ccso.fsOperations().fileTree(this.ccso.fsOperations().files(pkgDirectories))
        for (String dir : pkgDirectories) {
            File nextPackageJson = new File(root, PACKAGE_JSON)
            if (nextPackageJson.exists()) {
                log.debug "Processing '${nextPackageJson}' for NPM dependencies"
                FileTree nextCollection = calculateInstallableFiles(nextPackageJson)
                if (nextCollection != null) {
                    tree += nextCollection
                }
            }
        }

        tree
    }

    /**
     * Creates a {@link NpmExecSpec} with default {@code node} & {@code npm-cli.js} locations.
     *
     * @param ccso Grolifant {@code ProjectOperations} instance to attach to specification.
     * @param nodeJS Node extension
     * @param npm NPM extension
     * @param forceNodeDefaultForNpm Set to {@code true} to use the default NPM shipped with Node, rather than the
     *   configured NPM version.
     * @return Execution specification.
     *
     * @since 4.0
     */
    static NpmExecSpec createNpmExecSpec(
        ConfigCacheSafeOperations ccso,
        NodeJSConfigCacheSafeOperations nodeJS,
        NpmConfigCacheSafeOperations npm,
        boolean forceNodeDefaultForNpm = false
    ) {
        final spec = new NpmExecSpec(
            nodeJS.executable,
            forceNodeDefaultForNpm ? nodeJS.npmCliJsProvider : npm.executable,
            ccso
        )
        spec.entrypoint {
            workingDir(npm.homeDirectoryProvider)
        }
        spec
    }

    /**
     * Creates an {@link NpmExecSpec} with default {@code node} & {@code npm-cli.js} locations and then configure
     * an appropriate default environment.
     *
     * @param ccso Grolifant {@code ProjectOperations} instance to attach to specification.
     * @param nodeJS Node extension
     * @param npm NPM extension
     * @param forceNodeDefaultForNpm Set to {@code true} to use the default NPM shipped with Node, rather than the
     *   configured NPM version.
     * @return Execution specification.
     *
     * @since 4.0
     */
    static NpmExecSpec createNpmExecSpecWithDefaultEnvironment(
        ConfigCacheSafeOperations ccso,
        NodeJSConfigCacheSafeOperations nodeJS,
        NpmConfigCacheSafeOperations npm,
        boolean forceNodeDefaultForNpm = false
    ) {
        final spec = createNpmExecSpec(ccso, nodeJS, npm, forceNodeDefaultForNpm)
        spec.entrypoint {
            addEnvironmentProvider(environmentProviderFromExtensions(nodeJS, npm))
        }
        spec
    }

    /**
     * Creates an environment based up on extensions.
     *
     * @param nodeJS NodeJSExtension.
     * @param npm NpmExtension.
     * @return Environment
     * @since 0.10
     */
    static Map<String, Object> environmentFromExtensions(
        NodeJSConfigCacheSafeOperations nodeJS,
        NpmConfigCacheSafeOperations npm
    ) {
        Map<String, Object> env = [:]
        env.putAll(nodeJS.environmentProvider.get())
        env.put('npm_config_userconfig', npm.localConfigProvider.map { it.absolutePath })
        env.put('npm_config_globalconfig', npm.globalConfigProvider.map { it.absolutePath })
        env
    }

    /**
     * Creates an environment based up on extensions.
     *
     * @param nodeJS NodeJSExtension.
     * @param npm NpmExtension.
     * @return Environment
     * @since 4.0
     */
    static Provider<Map<String, String>> environmentProviderFromExtensions(
        NodeJSConfigCacheSafeOperations nodeJS,
        NpmConfigCacheSafeOperations npm
    ) {
        npm.localConfigProvider.zip(npm.globalConfigProvider) { lcp, gcp ->
            [
                npm_config_userconfig  : lcp.absolutePath,
                npm_config_globalconfig: gcp.absolutePath
            ]
        }.zip(nodeJS.environmentProvider) { cfg, node ->
            (node + cfg)
        }
    }

    private static String updateEnvPath(Map<String, Object> env) {
        final pathVar = OS.pathVar
        if (OS.unix) {
            String shPath = OS.findAllInPath('sh').first().parentFile.absolutePath
            if (env.containsKey(pathVar)) {
                env[pathVar] = "${env[pathVar]}${OS.pathSeparator}${shPath}"
            } else {
                env[pathVar] = shPath
            }
        } else if (OS.windows) {
            String cmdPath = OS.findAllInPath('cmd.exe').first().parentFile.absolutePath
            if (env.containsKey(pathVar)) {
                env[pathVar] = "${env[pathVar]}${OS.pathSeparator}${cmdPath}"
            } else {
                env[pathVar] = cmdPath
            }
        }
    }

    private final NodeJSConfigCacheSafeOperations nodeJS
    private final NpmConfigCacheSafeOperations npm
    private final ConfigCacheSafeOperations ccso
    private final boolean forceNodeDefaultForNpm

    private static final OperatingSystem OS = OperatingSystem.current()
}
