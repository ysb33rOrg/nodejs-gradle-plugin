/**
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.nodejs;

import org.gradle.api.provider.Provider;
import org.ysb33r.gradle.nodejs.internal.npm.SimpleNpmConfigCacheSafeOperations;

import java.io.File;

/**
 * NPM operations that should be configuration cache-safe.
 *
 * @since 5.0
 */
public interface NpmConfigCacheSafeOperations {

    /**
     * Creates a new instance from something else that imnplements the same interface.
     *
     * <p>
     *     THis is ueful to ensure configuration cache safety.
     * </p>
     * @param other Other implementing instance.
     * @return Instance with just the necessary values.
     */
    static NpmConfigCacheSafeOperations from(NpmConfigCacheSafeOperations other) {
        return new SimpleNpmConfigCacheSafeOperations(other);
    }

    /**
     * A provider for a resolved executable.
     *
     * @return File provider.
     */
    Provider<File> getExecutable();

    /**
     * The location of NPX.
     *
     * @return A provider to the location of NPX
     */
    Provider<File> getNpxCliJsProvider();

    /**
     * Location and name of global NPM config file.
     *
     * When this extension is attached to a project, the default location is set to
     * {@code "${project.gradle.gradleUserHomeDir}/npmrc"}
     *
     * @return {@link File} object pointing to global NPM config
     */
    Provider<File> getGlobalConfigProvider();

    /**
     * Location and name of local NPM config file.
     *
     * When this extension is attached to a project, the default location is set to
     * {@code "${project.rootProject.projectDir}/npmrc"}
     *
     * @return {@link File} object pointing to local NPM config
     */
    Provider<File> getLocalConfigProvider();

    /**
     * Location of {@code .npmrc} for a project.
     *
     * @return Project config level.
     */
    Provider<File> getProjectConfigProvider();

    /**
     * The NPM home directory - the parent directory of {@code node_modules},
     *
     * @return Parent directory of {@code node_modules}. Never null if the extension is tied to a project,
     * in which case it defaults to {@code project.projectDir}.
     */
    Provider<File> getHomeDirectoryProvider();

    /**
     * Provides the version as specified in {@code package.json}.
     *
     * @return Version provider.
     */
    Provider<String> getVersionProvider();
}
