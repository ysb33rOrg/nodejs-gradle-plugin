/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.nodejs.tasks

import groovy.transform.CompileStatic
import org.gradle.api.provider.Provider
import org.ysb33r.gradle.nodejs.NodeJSExtension
import org.ysb33r.gradle.nodejs.NpmConfigCacheSafeOperations
import org.ysb33r.gradle.nodejs.NpmExtension
import org.ysb33r.grolifant5.api.core.wrappers.AbstractWrapperCacheBinaryTask

import static org.ysb33r.gradle.nodejs.internal.Downloader.OS

/**
 * Caches node binaries
 *
 * @since 0.9.0
 */
@CompileStatic
class NodeBinariesCacheTask extends AbstractWrapperCacheBinaryTask {
    public static final String PROPERTIES_FILENAME = OS.windows ? 'node-wrapper.bat' : 'node-wrapper.properties'

    NodeBinariesCacheTask() {
        super(PROPERTIES_FILENAME)
        final nodejs = project.extensions.getByType(NodeJSExtension)
        final npm = project.extensions.getByType(NpmExtension)
        this.binaryLocation = nodejs.executable.map { File it ->
            it.canonicalPath
        }
        this.binaryVersionProvider = nodejs.resolvedExecutableVersion()
        this.npm = NpmConfigCacheSafeOperations.from(npm)
        this.projectName = project.name
    }

    /**
     * Obtains location of executable binary or script
     *
     * @return Location of executable as a string
     */
    @Override
    protected Provider<String> getBinaryLocationProvider() {
        this.binaryLocation
    }

    /**
     * Obtains version of binary or script
     *
     * @return Version as a string. Can be {@code null}.
     */
    @Override
    protected Provider<String> getBinaryVersionProvider() {
        this.binaryVersionProvider
    }

    /**
     * Obtains a description to be added to the cached binary properties file.
     *
     * @return Description. Never {@code null}.
     */
    @Override
    protected String getPropertiesDescription() {
        "Describes node/npm/npx usages for the ${projectName} project"
    }

    @Override
    protected Map<String, String> getAdditionalProperties() {
        super.additionalProperties + [
            NPM_LOCATION           : npm.executable.get().canonicalPath,
            NPX_LOCATION           : npm.npxCliJsProvider.get().canonicalPath,
            NPM_CONFIG_USERCONFIG  : npm.localConfigProvider.get().absolutePath,
            NPM_CONFIG_GLOBALCONFIG: npm.globalConfigProvider.get().absolutePath
        ]
    }

    private final NpmConfigCacheSafeOperations npm
    private final Provider<String> binaryLocation
    private final Provider<String> binaryVersionProvider
    private final String projectName
}