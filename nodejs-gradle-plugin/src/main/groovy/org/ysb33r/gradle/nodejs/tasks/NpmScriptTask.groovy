/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.nodejs.tasks

import groovy.transform.CompileStatic
import org.gradle.api.Action
import org.ysb33r.gradle.nodejs.NodeJSExtension
import org.ysb33r.gradle.nodejs.NpmExtension
import org.ysb33r.gradle.nodejs.NpmScriptExecSpec
import org.ysb33r.grolifant5.api.core.executable.BaseScriptDefinition
import org.ysb33r.grolifant5.api.core.runnable.AbstractExecTask

import static org.ysb33r.gradle.nodejs.utils.npm.NpmExecutor.environmentFromExtensions

/**
 * Runs scripts that are defined in {@code package.json}.
 *
 * @since 0.9.0
 */
@CompileStatic
class NpmScriptTask extends AbstractExecTask<NpmScriptExecSpec> {
    NpmScriptTask() {
        super()
        final npmExtension = extensions.create(NpmExtension.NAME, NpmExtension, this)
        final nodeExtension = extensions.create(NodeJSExtension.NAME, NodeJSExtension, this)
        execSpec = new NpmScriptExecSpec(nodeExtension.executable, npmExtension.executable, this)
        entrypoint {
            workingDir(npmExtension.homeDirectoryProvider)
            environment = environmentFromExtensions(nodeExtension, npmExtension)
        }
    }

    void script(Action<BaseScriptDefinition> configurator) {
        execSpec.script(configurator)
    }

    void script(@DelegatesTo(BaseScriptDefinition) Closure<?> configurator) {
        execSpec.script(configurator)
    }

    /**
     * Access to the underlying {@link org.ysb33r.gradle.nodejs.NpmExecSpec}.
     *
     * @return NPM execution specification.
     *
     * @since 2.0
     */
    @Override
    protected NpmScriptExecSpec getNativeExecSpec() {
        this.execSpec
    }

    private final NpmScriptExecSpec execSpec
}
