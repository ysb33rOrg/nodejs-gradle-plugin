/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.nodejs

import groovy.transform.CompileStatic
import groovy.util.logging.Slf4j
import org.gradle.api.Project
import org.gradle.api.Task
import org.gradle.api.UnknownDomainObjectException
import org.gradle.api.provider.Property
import org.gradle.api.provider.Provider
import org.ysb33r.gradle.nodejs.errors.BadNvmRcException
import org.ysb33r.grolifant5.api.core.runnable.ProvisionedExecMethods
import org.ysb33r.grolifant5.api.errors.UnsupportedConfigurationException

/** Configure project defaults or task specifics for Node.js.
 *
 * @since 0.1
 */
@CompileStatic
@Slf4j
class NodeJSExtension extends BaseNodeJSExtension<NodeJSExtension>
    implements ProvisionedExecMethods<NodeJSExecSpec>, NodeJSExtensionMethods {

    public static final String NAME = 'nodejs'

    /**
     * A provider to whether the node executable is resolved by version.
     *
     * @since 4.0
     */
    final Provider<Boolean> provideResolveNodeByVersion

    /**
     * A provider to whether the node version is synchronized form {@code .nvmrc}.
     *
     * @since 4.0
     */
    final Provider<Boolean> provideSyncNodeVersionFromNvmRc

    /** Constructs a new extension which is attached to the provided project.
     *
     * @param project Project this extension is associated with.
     */
    NodeJSExtension(Project project) {
        super(project)
        this.nvmrcLocation = project.objects.property(File)
        this.nvmrcLocation.set(defaultNvmrcLocation)
        this.provideResolveNodeByVersion = project.provider { -> owner.nodeVersionSet }
        this.provideSyncNodeVersionFromNvmRc =  project.provider { -> owner.syncFromNvmRc }
    }

    /**
     * Constructs a new extension which is attached to the provided task.
     *
     * @param project Project this extension is associated with.
     */
    NodeJSExtension(Task task) {
        super(task, task.project.extensions.getByType(NodeJSExtension))
        this.nvmrcLocation = task.project.objects.property(File)
        this.nvmrcLocation.set(defaultNvmrcLocation)
        this.provideResolveNodeByVersion = task.project.provider { -> owner.nodeVersionSet }
        this.provideSyncNodeVersionFromNvmRc =  task.project.provider { -> owner.syncFromNvmRc }
    }

    /**
     * Locate an executable by version, probably downloading it if not local.
     *
     * @param version Something resolvable to a string.
     */
    @Override
    void executableByVersion(Object version) {
        super.executableByVersion(version)
        syncFromNvmRc = false
        nodeVersionSet = true
    }

    /**
     * Locate an executable by a local path.
     *
     * @param path Something resolvable to a {@link File}.
     */
    @Override
    void executableByPath(Object path) {
        super.executableByPath(path)
        syncFromNvmRc = false
        nodeVersionSet = false
    }

    /**
     * Locate executable by searching the current environmental search path.
     *
     * @param baseName The base name of the executable
     */
    @Override
    void executableBySearchPath(Object baseName) {
        super.executableBySearchPath(baseName)
        syncFromNvmRc = false
        nodeVersionSet = false
    }

    /**
     * Read the version from {@code .nvmrc}.
     * Only supports fixed versions, not open versions like {@code node} or {@code lts/*}.
     *
     * Assumes {@code .nvmrc} is at the {@code npm} homedirectory
     *
     * @since 0.11
     */
    @SuppressWarnings('DuplicateStringLiteral')
    void executableByNvmrc() {
        try {
            executableByNvmRc(defaultNvmrcLocation)
        } catch (UnknownDomainObjectException | UnsupportedConfigurationException e) {
            throw new BadNvmRcException(
                'No NpmExtension was found. This is probably due to \'org.ysb33r.nodejs.npm\' plugin not installed.',
                e
            )
        }
        syncFromNvmRc = true
        nodeVersionSet = false
    }

    /**
     * Read the version from {@code .nvmrc}.
     * Only supports fixed versions, not open versions like {@code node} or {@code lts/*}.
     *
     * @param nvmrc Provider to the location of {@code .nvmrc}
     *
     * @since 0.11
     */
    @SuppressWarnings('CatchArrayIndexOutOfBoundsException')
    void executableByNvmRc(Provider<File> nvmrc) {
        if (nvmrc.present) {
            executableByVersion(nvmrc.map { File rc ->
                try {
                    String versionLine = rc.readLines()[0]?.trim()
                    if (!versionLine?.matches(~/^\d+\.\d+\.\d+.*$/)) {
                        throw new BadNvmRcException(
                            "'${versionLine}' is not an acceptable version. Gradle only accepts fixed versions."
                        )
                    }
                    versionLine
                } catch (ArrayIndexOutOfBoundsException e) {
                    throw new BadNvmRcException("${rc.absolutePath} seems to be empty", e)
                }
            })
            nvmrcLocation.set(nvmrc)
            syncFromNvmRc = true
            nodeVersionSet = false
        } else {
            throw new BadNvmRcException('No .nvmrc location is set or no NpmExtension is available')
        }
    }

    /**
     * Create execution specification.
     *
     * @return Execution specification.
     *
     * @since 0.10
     */
    @Override
    NodeJSExecSpec createExecSpec() {
        new NodeJSExecSpec(projectOperations, executable)
    }

    /**
     * {@code .nvmrc} location.
     *
     * @return Location of {@code .nvmrc}. Can be a null provider.
     *
     * @since 2.0
     */
    Provider<File> getNvmrcLocation() {
        this.nvmrcLocation
    }

    /**
     * Indicates whether the Node version is being synchronised from {@code .nvmrc}.
     *
     * @return {@code true} us linkage has been established.
     */
    boolean isSyncNodeVersionFromNvmRc() {
        this.syncFromNvmRc
    }

    /**
     * Indicates whether resolving {@code node} is by version.
     * @return {@code true} if the version has been set.
     */
    boolean isResolveNodeByVersion() {
        this.nodeVersionSet
    }

    /**
     * Constructs a new extension which is attached to the provided task.
     *
     * @param project Project this extension is associated with.
     * @param altProjectExt Alternative extension which implements NodeJSExtension.
     */
    protected NodeJSExtension(Task task, NodeJSExtension altProjectExt) {
        super(task, altProjectExt)
        this.nvmrcLocation = task.project.objects.property(File)
        this.nvmrcLocation.set(defaultNvmrcLocation)
        this.provideResolveNodeByVersion = task.project.provider { -> owner.nodeVersionSet }
        this.provideSyncNodeVersionFromNvmRc =  task.project.provider { -> owner.syncFromNvmRc }
    }

    private Provider<File> getDefaultNvmrcLocation() {
        projectOperations.providerTools.flatMap(projectOperations.provider(locateNpm)) {
            it.homeDirectoryProvider.map { new File(it, NVMRC) }
        }
    }

    private boolean syncFromNvmRc = false
    private boolean nodeVersionSet = true
    private final Property<File> nvmrcLocation
    private static final String NVMRC = '.nvmrc'
}
