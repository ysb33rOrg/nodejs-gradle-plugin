/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.nodejs.tasks

import groovy.transform.CompileDynamic
import groovy.transform.CompileStatic
import org.gradle.api.provider.Provider
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.Internal
import org.ysb33r.gradle.nodejs.BaseNodeJSExtension
import org.ysb33r.gradle.nodejs.BaseNpmExtension
import org.ysb33r.gradle.nodejs.NodeJSExecSpec
import org.ysb33r.gradle.nodejs.utils.npm.NpmExecutor
import org.ysb33r.grolifant5.api.core.runnable.GrolifantDefaultTask

import static org.ysb33r.gradle.nodejs.utils.NodeJSExecutor.createNodeJSExecSpecWithDefaultEnvironment

/**
 * A base class that will provide NodeJS and NPM task extensions.
 *
 * @since 2.1
 */
@CompileStatic
abstract class GenericNodeBaseTask<T extends BaseNodeJSExtension<T>, S extends BaseNpmExtension<T, S>>
    extends GrolifantDefaultTask {

    protected GenericNodeBaseTask() {
        super()
    }

    void environment(Map<String, ?> env) {
        updateEnvironmentOnNodeExtension(env, false)
    }

    void setEnvironment(Map<String, ?> env) {
        updateEnvironmentOnNodeExtension(env, true)
    }

    @Input
    abstract Provider<Map<String, String>> getEnvironment()

    /**
     * Internal access to attached NPM extension.
     * <p>
     *     Cannot be called from task actions.
     * </p>
     *
     * @return NPM extension.
     */
    @Internal
    abstract protected S getNpmExtension()

    /**
     * Internal access to attached NodeJS extension.
     * <p>
     *     Cannot be called from task actions.
     * </p>
     * @return NodeJS extension.
     */
    @Internal
    abstract protected T getNodeExtension()

    @Internal
    abstract protected NpmExecutor getNpmExecutor()

    /**
     * Creates a node.js execution specification and populates it with default
     * working directory, environment and location of {@code node}.
     *
     * <p>
     *     Cannot be called from within a task action if configuration cache is enabled.
     *     </p>
     * @return Pre-configured execution specification.
     */
    protected NodeJSExecSpec createExecSpec() {
        BaseNodeJSExtension<T> ext = nodeExtension
        final execSpec = createNodeJSExecSpecWithDefaultEnvironment(ext)
        final env = environment
        execSpec.entrypoint {
            workingDir = npmExtension.homeDirectoryProvider
            addEnvironmentProvider(env)
        }
        execSpec
    }

    @CompileDynamic
    private void updateEnvironmentOnNodeExtension(Map<String, ?> env, boolean clearFirst) {
        if (clearFirst) {
            nodeExtension.environment = env
        } else {
            nodeExtension.environment(env)
        }
    }
}
