/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.nodejs

import groovy.transform.CompileStatic
import org.gradle.api.provider.Provider
import org.ysb33r.grolifant5.api.core.ConfigCacheSafeOperations
import org.ysb33r.grolifant5.api.core.ProjectOperations
import org.ysb33r.grolifant5.api.core.runnable.AbstractScriptExecSpec

/** Execution specification for running a Node.js script.
 *
 * @since 0.1
 */
@CompileStatic
class NodeJSExecSpec extends AbstractScriptExecSpec<NodeJSExecSpec> {
    /**
     * Construct class and attach it to specific project.
     *
     * @param projectOperations Project this exec spec is attached to.
     * @param nodeLocation Location of Node executable.
     *
     * @since 2.0
     */
    NodeJSExecSpec(ProjectOperations projectOperations, Provider<File> nodeLocation) {
        super(ConfigCacheSafeOperations.from(projectOperations))

        entrypoint {
            executable(nodeLocation.map { it.absolutePath })
        }
    }
}
