/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.nodejs

import groovy.transform.CompileStatic
import org.gradle.api.Transformer
import org.gradle.api.provider.Provider
import org.ysb33r.grolifant5.api.core.ConfigCacheSafeOperations
import org.ysb33r.grolifant5.api.core.ProjectOperations
import org.ysb33r.grolifant5.api.core.executable.BaseScriptDefinition

/**
 * A specification for executing NPM scripts.
 *
 * @author Schalk W. Cronjé
 */
@CompileStatic
class NpmScriptDefinition extends BaseScriptDefinition {

    /**
     * An NPM script definition.
     *
     * @param po Configuration cache-safe operations.
     *
     * @since 4.0
     */
    NpmScriptDefinition(ConfigCacheSafeOperations po) {
        super(po)
        this.preArgsProvider = name.map(new Transformer<List<String>, String>() {
            @Override
            List<String> transform(String s) {
                args.empty ? [s] : [s, '--']
            }
        })
    }

    // Deprecated since 4.0
    @Deprecated
    NpmScriptDefinition(ProjectOperations po) {
        super(ConfigCacheSafeOperations.from(po))
        this.preArgsProvider = name.map(new Transformer<List<String>, String>() {
            @Override
            List<String> transform(String s) {
                args.empty ? [s] : [s, '--']
            }
        })
    }

    @Override
    Provider<List<String>> getPreArgs() {
        this.preArgsProvider
    }

    private final Provider<List<String>> preArgsProvider
}
