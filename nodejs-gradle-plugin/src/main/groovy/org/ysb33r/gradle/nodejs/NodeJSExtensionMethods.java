/**
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.nodejs;

import org.gradle.api.provider.Provider;

import java.util.Map;

/**
 * NodeJS methods that an extension should implement.
 *
 * @since 2.1
 */
public interface NodeJSExtensionMethods extends NodeJSConfigCacheSafeOperations {

    /**
     * Replaces current environment with new one.
     * If this is called on the task extension, no project extension environment will
     * be used.
     *
     * @param args New environment key-value map of properties.
     */
    void setEnvironment(Map<String, ?> args);

    /**
     * Environment for running the exe
     *
     * <p> Calling this will resolve all lazy-values in the variable map.
     *
     * @return Map of environmental variables that will be passed.
     */
    Map<String, String> getEnvironment();

    /**
     * Add environmental variables to be passed to the exe.
     *
     * @param args Environmental variable key-value map.
     */
    void environment(Map<String, ?> args);

    /**
     * Adds the system path to the execution environment.
     */
    void useSystemPath();

    /**
     * Add search to system path.
     * <p>
     * In the case of a task this will be appended to both the task and project extension's version
     * of the system path.
     *
     * @param postfix Provider of a path item that can be appended to the current system path.
     */
    void appendPath(Provider<String> postfix);

    /**
     * Add search to system path
     * <p>
     * In the case of a task this will be prefixed to both the task and project extension's version
     * of the system path.
     *
     * @param prefix Provider of a path item that can be prefixed to the current system path.
     */
    void prefixPath(Provider<String> prefix);
}
