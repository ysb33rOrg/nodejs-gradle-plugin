/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.nodejs.plugins

import groovy.transform.CompileStatic
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.provider.Provider
import org.ysb33r.gradle.nodejs.NpmExtension
import org.ysb33r.gradle.nodejs.PackageJson
import org.ysb33r.gradle.nodejs.tasks.NpmScriptTask
import org.ysb33r.grolifant5.api.core.executable.BaseScriptDefinition

/** Conventions for building a project with Node and Gradle.
 *
 * <ul>
 *   <li>All Node related code goes in {@code src/node}. This includes {@code package.json}</li>
 *   <li>Tasks are added to deal with {@code package.json}
 *   <li>Wrappers can be generated a project level and will point to src in {@code src/node}
 *   <li>Scripts are prefixed with {@code npmRun}.
 * </ul>
 *
 * @since 0.9.0
 */
@CompileStatic
class NpmDevPlugin implements Plugin<Project> {

    public static final String SCRIPT_TASK_PREFIX = 'npmRun'

    @Override
    void apply(Project project) {
        project.pluginManager.identity {
            apply(DevBasePlugin)
        }
        NpmExtension npm = project.extensions.getByType(NpmExtension)
        addRuleForScriptTasks(project, npm.homeDirectoryProvider.map { new File(it, 'package.json') })
    }

    private void addRuleForScriptTasks(Project project, Provider<File> packageJsonFileProvider) {
        project.tasks.addRule(
            "Pattern: ${SCRIPT_TASK_PREFIX}<Script>: Runs script defined in package.json"
        ) { String reqName ->
            if (reqName.startsWith(SCRIPT_TASK_PREFIX)) {
                project.logger.debug("Checking whether ${SCRIPT_TASK_PREFIX}<Script> can be applied to ${reqName}")
                File pj = packageJsonFileProvider.get()
                if (pj.exists()) {
                    final key = reqName.replaceFirst(SCRIPT_TASK_PREFIX, '').uncapitalize()
                    PackageJson metadata = PackageJson.parsePackageJson(pj)
                    if (metadata.scripts.containsKey(key)) {
                        NpmScriptTask run = project.tasks.create(reqName, NpmScriptTask)
                        run.script { BaseScriptDefinition spec -> spec.name = key }
                        run.mustRunAfter DevBasePlugin.INSTALL_TASK
                    } else {
                        project.logger.debug("No script matches ${key}. Ignoring rule for ${reqName}")
                    }
                } else {
                    project.logger.debug("${pj} does not exist. Ignoring rule for ${reqName}")
                }
            }
        }
    }
}

