/**
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.nodejs;

import org.gradle.api.provider.Provider;

import java.io.File;

/**
 * Methods than Npm extension is required to implement.
 *
 * @since 2.1
 */
public interface NpmExtensionMethods extends NpmConfigCacheSafeOperations {

    /**
     * Use the NPM that is bundled with Node.
     * <p>
     * This is the default and should rarely be required.
     */
    void executableIsBundled();

    /**
     * Create execution specification.
     *
     * @return Execution specification.
     */
    NpmExecSpec createExecSpec();

    /**
     * Location and name of global NPM config file.
     *
     * When this extension is attached to a project, the default location is set to
     * {@code "${project.gradle.gradleUserHomeDir}/npmrc"}
     *
     * @return {@link java.io.File} object pointing to global NPM config
     */
    File getGlobalConfig();

    /**
     * Sets global config file.
     *
     * @param path Anything that can be converted using {@code project.file}.
     */
    void setGlobalConfig(Object path);

    /**
     * Sets global config file.
     *
     * @param path Anything that can be converted using {@code project.file}.
     */
    void globalConfig(Object path);

    /**
     * Location and name of local NPM config file.
     *
     * When this extension is attached to a project, the default location is set to
     * {@code "${project.rootProject.projectDir}/npmrc"}
     *
     * @return {@link java.io.File} object pointing to local NPM config
     */
    File getLocalConfig();

    /**
     * Location of {@code .npmrc} for a project.
     *
     * @return Project config level.
     */
    Provider<File> getProjectConfig();

    /**
     * Sets local config file.
     *
     * @param path Anything that can be converted using {@code project.file}.
     */
    void setLocalConfig(Object path);

    /**
     * Sets local config file.
     *
     * @param path Anything that can be converted using {@code project.file}.
     */
    void localConfig(Object path);

    /**
     * The NPM home directory - the parent directory of {@code node_modules},
     *
     * @return Parent directory of {@code node_modules}. Never null if the extension is tied to a project,
     * in which case it defaults to {@code project.projectDir}.
     */
    File getHomeDirectory();

    /**
     * Sets the home directory.
     *
     * @param homeDir A directory in which {@code node_modules} will be created as a child folder.
     *                Anything that can be resolved with {@code project.file} is acceptable
     */
    void setHomeDirectory(Object homeDir);

    /**
     * Sets the home directory.
     *
     * @param homeDir A directory in which {@code node_modules} will be created as a child folder.
     *                Anything that can be resolved with {@code project.file} is acceptable
     */
    void homeDirectory(Object homeDir);

    /**
     * Tries to determine the location where global config will typically be placed.
     *
     * @return A provider to a system location.
     */
    Provider<File> useGlobalSystemLocation();

    /**
     * Sets it to use the equivalent of {@code getGradleHomeDir()/.npmrc}.
     * <p>
     * This allows Gradle projects to be custom configured on local system without being affected by any system-wide
     * node installations.
     * </p>
     *
     * @return Location of a global configuration file for NPM that is located inside the Gradle home directory.
     */
    Provider<File> useGlobalGradleLocation();

    /**
     * Use to indicate that userconfig should be retrieved from the user's home directory.
     * <p>
     * Typically, this is {@code ~/.npmrc} or {@code %APPDATA\npm\.npmrc}
     * </p>
     *
     * @return Location for a userconfig {@code .npmrc}
     */
    Provider<File> useLocalUserProfileLocation();

    /**
     * Use to indicate that userconfig should be retrieved from the NPM home directory as configured by
     * {@link #getHomeDirectory()}.
     *
     * @return Location for a userconfig {@code .npmrc}
     */
    Provider<File> useLocalProjectLocation();

    /**
     * Use the version specified in {@code package.json} as the version of the project.
     * <p>
     * If {@code package.json} does not exist, then {@code undefined} is used as the version.
     */
    void usePackageJsonVersionAsProjectVersion();
}
