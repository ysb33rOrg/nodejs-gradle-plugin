/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.nodejs.internal.npm

import groovy.transform.CompileStatic
import org.gradle.api.provider.Provider
import org.ysb33r.grolifant5.api.core.OperatingSystem
import org.ysb33r.grolifant5.api.core.ProjectOperations

/**
 * Provides locations of various {@code .npmrc} files.
 *
 * @since 2.1
 */
@CompileStatic
class NpmrcProviders {

    public static final List<String> NPMRC_SEARCH_LOCATIONS = [
        '/etc',
        '/usr/local/etc',
        '/etc/npm',
        '/usr/local/etc/npm'
    ].asImmutable()

    /**
     * On Windows this is sually at {@code %APPDATA%/Roaming/npm/npmrc}.
     * On other systems it will be a file {@code npmrc} which will typically reside in one fo the directories specified
     * by {@link #NPMRC_SEARCH_LOCATIONS}
     */
    final Provider<File> system

    /**
     * Typically {@code ~/.gradle/.npmrc}
     */
    final Provider<File> gradle

    /**
     * On Windows it will be found as {@code %APPDATA%/npm/.npmrc}.
     * On other systems it will be {@code ~/.npmrc}.
     */
    final Provider<File> user

    /**
     * This is typically in {@code src/node/.npmrc}, but it is dependant on the where the {@code npmHomeProvider}
     * points to.
     */
    final Provider<File> project

    NpmrcProviders(
        ProjectOperations po,
        File gradleUserHomeDir,
        Provider<File> npmHomeProvider
    ) {
        File userLocation = new File(gradleUserHomeDir, NPM_USER_CONFIG)
        boolean windows = OperatingSystem.current().windows
        Provider<String> dataEnvVar = po.environmentVariable(
            windows ? 'APPDATA' : 'HOME',
            po.atConfigurationTime()
        )

        gradle = po.provider { -> userLocation }

        user = dataEnvVar.map { String envValue ->
            windows ? new File(envValue, "npm/${NPM_USER_CONFIG}") : new File(envValue, NPM_USER_CONFIG)
        }

        system = windows ? dataEnvVar.map { String envValue ->
            new File(envValue, "Roaming/npm/${NPM_CONFIG}")
        } : po.provider { ->
            String atDir = NPMRC_SEARCH_LOCATIONS.find {
                new File(it, NPM_CONFIG).exists()
            }

            if (!atDir) {
                throw new FileNotFoundException(
                    "Global ${NPM_CONFIG} not found in any of ${NPMRC_SEARCH_LOCATIONS}"
                )
            }

            new File(atDir, NPM_CONFIG)
        }

        project = npmHomeProvider.map { File it -> new File(it, NPM_USER_CONFIG) }
    }

    private static final String NPM_CONFIG = 'npmrc'
    private static final String NPM_USER_CONFIG = '.npmrc'
}
