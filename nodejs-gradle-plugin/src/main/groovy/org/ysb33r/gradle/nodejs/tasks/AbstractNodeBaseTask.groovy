/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.nodejs.tasks

import groovy.transform.CompileStatic
import org.gradle.api.provider.Provider
import org.ysb33r.gradle.nodejs.NodeJSExecSpec
import org.ysb33r.gradle.nodejs.NodeJSExtension
import org.ysb33r.gradle.nodejs.NpmExtension
import org.ysb33r.gradle.nodejs.utils.npm.NpmExecutor
import org.ysb33r.grolifant5.api.core.runnable.ConfigCacheSafeExecMethods
import org.ysb33r.grolifant5.api.core.runnable.ExecOutput

/**
 * A base class that will provide NodeJS and NPM task extensions.
 *
 * @since 0.1
 */
@CompileStatic
class AbstractNodeBaseTask extends GenericNodeBaseTask<NodeJSExtension, NpmExtension> {

    protected AbstractNodeBaseTask() {
        super()
        extensions.create(NpmExtension.NAME, NpmExtension, this)
        final nodejs = extensions.create(NodeJSExtension.NAME, NodeJSExtension, this)
        this.env = nodejs.environmentProvider
        npmExecutor = new NpmExecutor(this, nodeExtension, npmExtension)
        execMethods = ConfigCacheSafeExecMethods.from(nodejs)
    }

    @Override
    Provider<Map<String, String>> getEnvironment() {
        this.env
    }

    /**
     * Internal access to attached NPM extension.
     *
     * @return NPM extension.
     */
    @Override
    protected NpmExtension getNpmExtension() {
        extensions.getByType(NpmExtension)
    }

    /**
     * Internal access to attached NodeJS extension.
     *
     * @return NodeJS extension.
     */
    @Override
    protected NodeJSExtension getNodeExtension() {
        extensions.getByType(NodeJSExtension)
    }

    protected NpmExecutor getNpmExecutor() {
        this.npmExecutor
    }

    /**
     * Executes a configured execution specification.
     *
     * @param execSpec Configured specification.
     * @return Execution result
     */
    protected ExecOutput runExecSpec(NodeJSExecSpec execSpec) {
        execMethods.exec(execSpec)
    }

    private final NpmExecutor npmExecutor
    private final ConfigCacheSafeExecMethods<NodeJSExecSpec> execMethods
    private final Provider<Map<String, String>> env
}
