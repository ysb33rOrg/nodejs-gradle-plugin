/**
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.nodejs;

import org.gradle.api.provider.Provider;
import org.ysb33r.gradle.nodejs.internal.node.SimpleNodeJSConfigCacheSafeOperations;

import java.io.File;
import java.util.Map;

/**
 * NodeJS methods that an extension should implement.
 *
 * @since 5.0
 */
public interface NodeJSConfigCacheSafeOperations {

    static NodeJSConfigCacheSafeOperations from(NodeJSConfigCacheSafeOperations other) {
        return new SimpleNodeJSConfigCacheSafeOperations(other);
    }

    /**
     * A provider for a resolved executable.
     *
     * @return File provider.
     */
    Provider<File> getExecutable();

    /**
     * Resolves a path to a {@code npm} executable that is associated with the configured Node.js.
     *
     * @return Returns the path to the located {@code npm} executable.
     */
    Provider<File> getNpmCliJsProvider();

    /**
     * Environment for running the exe
     *
     * <p> Calling this will resolve all lazy-values in the variable map.
     *
     * @return Map of environmental variables that will be passed.
     *
     */
    Provider<Map<String, String>> getEnvironmentProvider();
}
