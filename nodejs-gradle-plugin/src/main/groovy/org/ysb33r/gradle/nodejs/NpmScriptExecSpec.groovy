/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.nodejs

import groovy.transform.CompileStatic
import org.gradle.api.Action
import org.gradle.api.provider.Provider
import org.ysb33r.grolifant5.api.core.ClosureUtils
import org.ysb33r.grolifant5.api.core.CmdlineArgumentSpec
import org.ysb33r.grolifant5.api.core.ConfigCacheSafeOperations
import org.ysb33r.grolifant5.api.core.ProjectOperations
import org.ysb33r.grolifant5.api.core.executable.BaseScriptDefinition
import org.ysb33r.grolifant5.api.core.runnable.AbstractExecSpec

/**
 * Base specification for running an NPM script via {@code npm-cli.js}
 *
 * <p> For simplicity Gradle executes {@code npm-cli.js} directly rather
 * than use yet another indirection of the {@code npm} shell script.</p>
 *
 * @since 2.0
 */
@CompileStatic
class NpmScriptExecSpec extends AbstractExecSpec<NpmScriptExecSpec> {

    /**
     * Construct class and attach it to specific project.
     *
     * @param nodeLocation Provider to the location of node.
     * @param npmCliJslocation Provider to the location of {@code npm.js}
     * @param ccso Configuration cache-safe operations.
     * @since 4.0
     */
    NpmScriptExecSpec(
        Provider<File> nodeLocation,
        Provider<File> npmCliJslocation,
        ConfigCacheSafeOperations ccso
    ) {
        super(ccso)
        this.npmCliJsLocation = npmCliJslocation
        this.scriptDefinition = new NpmScriptDefinition(ccso)
        this.emptyList = ccso.providerTools().provider { -> Collections.EMPTY_LIST }

        entrypoint {
            executable(nodeLocation)
        }

        runnerSpec {
            args(npmCliJsLocation.map { x -> x.absolutePath })
            args('--scripts-prepend-node-path=true')
        }

        addCommandLineProcessor('run', 1, new RunArgumentSpec(this))
        addCommandLineProcessor('script', 2, this.scriptDefinition)
    }

    /**
     * Construct class and attach it to specific project.
     *
     * @param objectFactory Gradle object factory.
     * @param projectOperations Project this exec spec is attached.
     * @since 2.0
     *
     * @deprecated
     */
    @Deprecated
    NpmScriptExecSpec(
        Provider<File> nodeLocation,
        Provider<File> npmCliJslocation,
        ProjectOperations projectOperations
    ) {
        super(ConfigCacheSafeOperations.from(projectOperations))
        this.npmCliJsLocation = npmCliJslocation
        this.scriptDefinition = new NpmScriptDefinition(ConfigCacheSafeOperations.from(projectOperations))
        this.emptyList = projectOperations.provider { -> Collections.EMPTY_LIST }

        entrypoint {
            executable(nodeLocation)
        }

        runnerSpec {
            args(npmCliJsLocation.map { x -> x.absolutePath })
            args('--scripts-prepend-node-path=true')
        }

        addCommandLineProcessor('run', 1, new RunArgumentSpec(this))
        addCommandLineProcessor('script', 2, this.scriptDefinition)
    }

    /**
     * Configures the script.
     *
     * @param configurator Configures the required script.
     */
    void script(Action<BaseScriptDefinition> configurator) {
        configurator.execute(this.scriptDefinition)
    }

    /**
     * Configures the script.
     *
     * @param configurator Configures the required script.
     */
    void script(@DelegatesTo(BaseScriptDefinition) Closure<?> configurator) {
        ClosureUtils.configureItem(this.scriptDefinition, configurator)
    }

    private final Provider<File> npmCliJsLocation
    private final BaseScriptDefinition scriptDefinition
    private final Provider<List<String>> emptyList

    private static class RunArgumentSpec implements CmdlineArgumentSpec {
        RunArgumentSpec(NpmScriptExecSpec spec) {
            this.spec = spec
        }

        @Override
        List<String> getArgs() {
            []
        }

        @Override
        void args(Object... args) {
        }

        @Override
        void args(Iterable<?> args) {
        }

        @Override
        void setArgs(Iterable<?> args) {
        }

        @Override
        void addCommandLineArgumentProviders(Provider<List<String>>... providers) {
        }

        @Override
        Provider<List<String>> getPreArgs() {
            spec.emptyList
        }

        @Override
        Provider<List<String>> getAllArgs() {
            spec.emptyList.map { ['run'] }
        }

        @Override
        List<Provider<List<String>>> getCommandLineArgumentProviders() {
            []
        }

        private final NpmScriptExecSpec spec
    }
}
