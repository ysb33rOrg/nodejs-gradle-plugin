/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.nodejs.tasks

import groovy.transform.CompileStatic
import org.gradle.api.provider.Provider
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.InputFile
import org.gradle.api.tasks.Internal
import org.gradle.api.tasks.OutputFile
import org.gradle.api.tasks.PathSensitive
import org.gradle.api.tasks.TaskAction
import org.ysb33r.gradle.nodejs.BaseNodeJSExtension
import org.ysb33r.gradle.nodejs.BaseNpmExtension
import org.ysb33r.gradle.nodejs.utils.npm.NpmPackageInstaller

import static org.gradle.api.tasks.PathSensitivity.RELATIVE
import static org.ysb33r.gradle.nodejs.plugins.DevBasePlugin.DEFAULT_GROUP

/**
 * Installs the packages as described by a {@code package.json} file
 *
 * @since 2.1
 */
@CompileStatic
@SuppressWarnings('AbstractClassWithoutAbstractMethod')
abstract class AbstractBaseNpmPackageJsonInstall<T extends BaseNodeJSExtension<T>, S extends BaseNpmExtension<T, S>>
    extends GenericNodeBaseTask<T, S> {

    /**
     * THe NPM home directory.
     *
     * @return A provider to the home directory.
     *
     * @since 4.0
     */
    @Internal
    abstract Provider<File> getHomeDirProvider()

    /** Set to {@code true} if only production dependencies should be installed.
     *
     * @since 0.9.0
     */
    @Internal
    boolean productionOnly = false

    /**
     * The package.json file that this task will query.
     *
     * @return File object.
     */
    @Internal
    File getPackageJsonFile() {
        packageJsonFileProvider.get()
    }

    /** The package.json file that this task will query.
     *
     * @return File provider object.
     *
     * @since 0.8.0
     */
    @PathSensitive(RELATIVE)
    @InputFile
    Provider<File> getPackageJsonFileProvider() {
        homeDirProvider.map { new File(it, 'package.json') }
    }

    /**
     * The executable-lock.json file that this task will create.
     *
     * @return File object.
     */
    @Internal
    File getPackageLockFile() {
        packageLockFileProvider.get()
    }

    /** The executable-lock.json file that this task will create.
     *
     * @return File object.
     *
     * @since 0.8.0
     */
    @OutputFile
    Provider<File> getPackageLockFileProvider() {
        homeDirProvider.map { new File(it, 'executable-lock.json') }
    }

    /** Replace list of arguments with a new list.
     *
     * @param args new arguments to use.
     *
     * @sa https://docs.npmjs.com/cli/install
     */
    void setAdditionalInstallArgs(Iterable<String> args) {
        this.additionalInstallArgs.clear()
        this.additionalInstallArgs.addAll(args)
    }

    /** Adds more installation arguments
     *
     * @param args One or more arguments
     *
     * @sa https://docs.npmjs.com/cli/install
     */
    void additionalInstallArgs(String... args) {
        additionalInstallArgs(args as List)
    }

    /** Adds more installation arguments
     *
     * @param args Iterable list of arguments
     *
     * @sa https://docs.npmjs.com/cli/install
     */
    void additionalInstallArgs(Iterable<String> args) {
        this.additionalInstallArgs.addAll(args)
    }

    /** Customise installation via additional argument that are passed to {@code npm install}.
     *
     * @return List of additional arguments.
     *
     * @sa https://docs.npmjs.com/cli/install
     */
    @Input
    Iterable<String> getAdditionalInstallArgs() {
        this.additionalInstallArgs
    }

    @TaskAction
    void exec() {
        npmPackageInstaller.run()
    }

    protected AbstractBaseNpmPackageJsonInstall() {
        super()
        group = DEFAULT_GROUP
    }

    /**
     * Returns the NPM packer installer instance.
     *
     * <p>
     *   This method is called from task actionsand thus should be configuration cache safe.
     * </p>
     * @return Package installer.
     *
     * @since 4.0
     */
    @Internal
    protected abstract NpmPackageInstaller getNpmPackageInstaller()

    /**
     * Creates an actual installer.
     *
     * The default method relies on {@link #getPackageJsonFileProvider} not returning null.
     *
     * @return An installer.
     *
     * @since 4.0
     */
    protected NpmPackageInstaller createPackageInstaller() {
        npmExecutor.createNpmPackageInstaller(
            packageJsonFileProvider,
            providerTools().provider { -> owner.additionalInstallArgs.asList() },
            providerTools().provider { -> owner.productionOnly }
        )
    }

    private final List<String> additionalInstallArgs = []
}
