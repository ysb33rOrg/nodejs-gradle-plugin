/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.nodejs.internal.npm

import groovy.transform.CompileStatic
import org.gradle.api.provider.Provider
import org.ysb33r.gradle.nodejs.NpmConfigCacheSafeOperations

/**
 * Npm operations that should be configuration-cache safe
 *
 * @author Schalk W. Cronjé
 *
 * @since 4.0
 */
@CompileStatic
class SimpleNpmConfigCacheSafeOperations implements NpmConfigCacheSafeOperations {
    final Provider<File> executable
    final Provider<File> npxCliJsProvider
    final Provider<File> globalConfigProvider
    final Provider<File> localConfigProvider
    final Provider<File> projectConfigProvider
    final Provider<File> homeDirectoryProvider
    final Provider<String> versionProvider

    SimpleNpmConfigCacheSafeOperations(NpmConfigCacheSafeOperations other) {
        this.executable = other.executable
        this.npxCliJsProvider = other.npxCliJsProvider
        this.globalConfigProvider = other.globalConfigProvider
        this.localConfigProvider = other.localConfigProvider
        this.projectConfigProvider = other.projectConfigProvider
        this.homeDirectoryProvider = other.homeDirectoryProvider
        this.versionProvider = other.versionProvider
    }
}
