/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.nodejs.plugins

import groovy.transform.CompileStatic
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.tasks.Delete
import org.ysb33r.gradle.nodejs.tasks.NpmPackageJsonInstall
import org.ysb33r.gradle.nodejs.tasks.SyncPackageJson
import org.ysb33r.grolifant5.api.core.ProjectOperations

/** Conventions for building a project with Node and Gradle, but driving everything from Gradle.
 *
 * <ul>
 *   <li>All Node related code goes in {@code src/node}.
 *   <li>{@code package.json} is exluded from {@code .gitignore}, becasue it will be generated</li>
 *   <li>Tasks are added to deal with generating {@code package.json}
 *   <li>Wrappers can be generated a project level and will point to src in {@code src/node}
 * </ul>
 *
 * @since 0.13.0
 */
@CompileStatic
class AltDevPlugin implements Plugin<Project> {
    public static final String SYNC_TASK = 'syncPackageJson'
    public static final String INSTALL_TASK = DevBasePlugin.INSTALL_TASK

    @Override
    void apply(Project project) {
        project.pluginManager.identity {
            apply(DevBasePlugin)
        }
        ProjectOperations po = ProjectOperations.find(project)
        addPackageJsonTasks(po, project)
//        addRuleForScriptTasks(project, install.packageJsonFileProvider)
    }

    private NpmPackageJsonInstall addPackageJsonTasks(ProjectOperations po, Project project) {
        po.tasks.register(SYNC_TASK, SyncPackageJson) { sync ->
        }

        po.tasks.whenNamed(INSTALL_TASK, NpmPackageJsonInstall) {
            it.dependsOn(SYNC_TASK)
        }

        final syncTask = po.tasks.taskProviderFrom(project.tasks, po.providers, SYNC_TASK)
        po.tasks.whenNamed(DevBasePlugin.CLEAN_TASK, Delete) {
            it.delete syncTask.map { it.outputs.files }
        }
    }
}

