/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.nodejs.pkgwrapper

import groovy.transform.CompileStatic
import groovy.util.logging.Slf4j
import org.gradle.api.Project
import org.gradle.api.Task
import org.gradle.api.plugins.ExtensionAware
import org.gradle.api.provider.Property
import org.gradle.api.provider.Provider
import org.ysb33r.gradle.nodejs.NodeJSExtension
import org.ysb33r.gradle.nodejs.NpmDependencyGroup
import org.ysb33r.gradle.nodejs.NpmExtension
import org.ysb33r.gradle.nodejs.internal.npm.TagBasedNpmPackageResolver
import org.ysb33r.grolifant5.api.core.ConfigCacheSafeOperations
import org.ysb33r.grolifant5.api.core.ProjectOperations
import org.ysb33r.grolifant5.api.core.runnable.CombinedProjectTaskExtensionBase

/**
 * A base class that can be used to wrap specific NPM packages as features in Gradle,
 *   thereby allowing for configuraton both a project or task level.
 *
 * @since 0.1
 */
@CompileStatic
@Slf4j
abstract class AbstractPackageWrappingExtension
    extends CombinedProjectTaskExtensionBase<AbstractPackageWrappingExtension> {

    /**
     * Set package to be resolved by version.
     *
     * @param ver Version. Anything that can be resolved to a valid version string.
     *
     * @since 0.10
     */
    void executableByVersion(Object ver) {
        this.exeProvider.set(packageResolver.resolverFromVersion(ver))
    }

    /**
     * Set package to be located by fixed path.
     *
     * @param path Path to package. Anything than can be resolved using
     *   {@link org.ysb33r.grolifant5.api.core.FileSystemOperations#file}.
     *
     * @since 0.10
     */
    void executableByPath(Object path) {
        this.exeProvider.set(packageResolver.resolverFromPath(path))
    }

    /**
     * An executable entry point for a package that is suitable for execution by {@code node}.
     *
     * @return Location of package entry point.
     *
     * @since 0.10
     */
    Provider<File> getExecutable() {
        this.exeProvider
    }

    /** Sets the installation group (production, development, optional).
     *
     * @param installGroup
     */
    void setInstallGroup(NpmDependencyGroup installGroup) {
        this.installGroup = installGroup
    }

    /** The installation group for this executable.
     *
     * @return The configured instalaltion group.
     *   If this is a task extension and it has not been configured, it will query the equivalent project extension.
     *   For a project extension that is not configured, {@code NpmDependencyGroup.PRODUCTION} will be returned
     */
    NpmDependencyGroup getInstallGroup() {
        if (this.task) {
            this.installGroup ?: globalPackageWrappingExtension.installGroup
        } else {
            this.installGroup
        }
    }

    /** Associate extension with a project and use default extensions.
     *
     * Defaults to {@link NpmDependencyGroup#PRODUCTION}.
     *
     * @param project Associated project
     * @param packageName Name of package
     */
    protected AbstractPackageWrappingExtension(Project project, String packageName) {
        super(ProjectOperations.find(project))
        this.installGroup = NpmDependencyGroup.PRODUCTION
        this.npmExtension = project.extensions.getByType(NpmExtension)
        this.nodeJSExtension = project.extensions.getByType(NodeJSExtension)
        this.packageResolver = createTagResolver(null, packageName)
        this.exeProvider = project.objects.property(File)
    }

    /**
     *
     * @param project Associated project
     * @param packageName Name of package
     * @param installGroup Install group
     * @param embeddedExtensions Whether embedded extensions should be used.
     * Default is not to create embedded extensions, but use the default extensions.
     */
    protected AbstractPackageWrappingExtension(
        Project project,
        String packageName,
        NpmDependencyGroup installGroup,
        boolean embeddedExtensions = USE_DEFAULT_EXTENSIONS
    ) {
        super(ProjectOperations.find(project))

        if (embeddedExtensions) {
            this.nodeJSExtension = ((ExtensionAware) this).extensions.create(
                NodeJSExtension.NAME,
                NodeJSExtension,
                project
            )
            this.npmExtension = ((ExtensionAware) this).extensions.create(
                NpmExtension.NAME,
                NpmExtension,
                project,
                this.nodeJSExtension
            )
        } else {
            this.npmExtension = project.extensions.getByType(NpmExtension)
            this.nodeJSExtension = project.extensions.getByType(NodeJSExtension)
        }

        this.installGroup = installGroup
        this.packageResolver = createTagResolver(null, packageName)
        this.exeProvider = project.objects.property(File)
    }

    /** Associate extension with a task.
     *
     * @param task
     * @param name Name by which project extension is known.
     * @deprecated
     */
    @Deprecated
    protected AbstractPackageWrappingExtension(Task task, final String name) {
        super(
            task,
            ProjectOperations.find(task.project),
            (AbstractPackageWrappingExtension) task.project.extensions.getByName(name)
        )
        this.npmExtension = task.extensions.getByType(NpmExtension)
        this.nodeJSExtension = task.extensions.getByType(NodeJSExtension)
        this.exeProvider = task.project.objects.property(File)
        this.exeProvider.set(((AbstractPackageWrappingExtension) task.project.extensions.getByName(name)).executable)
    }

    /** Associate projExt with a task.
     *
     * @param task
     * @param projExt The project projExt.
     * @param embeddedExtensions Whether embedded extensions should be used.
     * Default is not to create embedded extensions, but use the default extensions.
     */
    protected AbstractPackageWrappingExtension(
        Task task,
        final AbstractPackageWrappingExtension projExt,
        boolean embeddedExtensions = USE_DEFAULT_EXTENSIONS
    ) {
        super(
            task,
            ProjectOperations.find(task.project),
            projExt
        )
        if (embeddedExtensions) {
            this.nodeJSExtension = ((ExtensionAware) this).extensions.create(
                NodeJSExtension.NAME,
                NodeJSExtension,
                task,
                ((ExtensionAware) projExt).extensions.getByType(NodeJSExtension)
            )
            this.npmExtension = ((ExtensionAware) this).extensions.create(
                NpmExtension.NAME,
                NpmExtension,
                task,
                this.nodeJSExtension,
                ((ExtensionAware) projExt).extensions.getByType(NpmExtension)
            )
        } else {
            this.npmExtension = task.extensions.getByType(NpmExtension)
            this.nodeJSExtension = task.extensions.getByType(NodeJSExtension)
        }
        this.exeProvider = task.project.objects.property(File)
        this.exeProvider.set(projExt.executable)
    }

    /**
     * Whether the NodeJS & Npm extensions should be created as extensions of this extensions and thus
     * be decoupled from the normal extensions provided by this plugin suite.
     *
     * This is very useful for wrapping Node-based tools, which can potentially be used independently from
     * any Javascript development.
     *
     * @since 0.10
     */
    protected static final boolean USE_EMBEDDED_EXTENSIONS = true

    /**
     * Whether the NodeJS & Npm extensions should be those provided by the project and tasals
     *
     * @since 0.10
     */
    protected static final boolean USE_DEFAULT_EXTENSIONS = false

    /** {@link NpmExtension} associated with the project or task extension
     *
     * @return {@link NpmExtension}
     */
    protected final NpmExtension npmExtension

    /** {@link NodeJSExtension} associated with the project or task extension
     *
     * @return {@link NodeJSExtension}
     */
    protected final NodeJSExtension nodeJSExtension

    /** The entrypoint path relative to the installed executable folder
     *
     * @return Relative path
     */
    abstract protected String getEntryPoint()

    /** Returns the name by which the extension is known.
     *
     * @return Extension name. Never null.
     *
     * @deprecated
     */
    @Deprecated
    abstract protected String getExtensionName()

    protected AbstractPackageWrappingExtension getGlobalPackageWrappingExtension() {
        (AbstractPackageWrappingExtension) projectExtension
    }

    private TagBasedNpmPackageResolver createTagResolver(final String scope, final String packageName) {
        new TagBasedNpmPackageResolver(
            scope,
            packageName,
            ConfigCacheSafeOperations.from(projectOperations),
            nodeJSExtension,
            npmExtension,
            this.&getInstallGroup,
            this.&getEntryPoint
        )
    }

    private NpmDependencyGroup installGroup
    private final TagBasedNpmPackageResolver packageResolver
    private final Property<File> exeProvider
}
