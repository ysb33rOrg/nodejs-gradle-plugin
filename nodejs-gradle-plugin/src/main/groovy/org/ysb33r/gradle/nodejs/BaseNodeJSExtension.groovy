/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.nodejs

import groovy.transform.CompileStatic
import org.gradle.api.Project
import org.gradle.api.Task
import org.gradle.api.UnknownDomainObjectException
import org.gradle.api.plugins.ExtensionContainer
import org.gradle.api.provider.ListProperty
import org.gradle.api.provider.MapProperty
import org.gradle.api.provider.Provider
import org.gradle.process.ExecSpec
import org.ysb33r.gradle.nodejs.internal.Downloader
import org.ysb33r.gradle.nodejs.utils.NodeJSExecutor
import org.ysb33r.grolifant5.api.core.ConfigCacheSafeOperations
import org.ysb33r.grolifant5.api.core.OperatingSystem
import org.ysb33r.grolifant5.api.core.ProjectOperations
import org.ysb33r.grolifant5.api.core.downloader.ExecutableDownloader
import org.ysb33r.grolifant5.api.core.runnable.AbstractToolExtension
import org.ysb33r.grolifant5.api.core.runnable.ProvisionedExecMethods
import org.ysb33r.grolifant5.api.errors.ConfigurationException
import org.ysb33r.grolifant5.internal.core.executable.ExecUtils

import java.util.concurrent.Callable
import java.util.function.BiFunction

import static java.util.Collections.EMPTY_LIST
import static java.util.Collections.EMPTY_MAP

/**
 * Base class for configuring project defaults or task specifics for Node.js.
 *
 * @author Schalk W. Cronjé
 * @since 2.0
 */
@CompileStatic
abstract class BaseNodeJSExtension<T extends BaseNodeJSExtension> extends AbstractToolExtension<T>
    implements NodeJSExtensionMethods, ProvisionedExecMethods<NodeJSExecSpec> {

    final ConfigCacheSafeOperations configCacheSafeOperations

    /** Resolves a path to a {@code npm} executable that is associated with the configured Node.js.
     *
     * @return Returns the path to the located {@code npm} executable.
     * @throw {@code ExecConfigurationException} if executable was not configured.
     *
     * @since 0.10
     */
    Provider<File> getNpmCliJsProvider() {
        this.npmCliProvider
    }

    /** Replace current environment with new one.
     * If this is called on the task extension, no project extension environment will
     * be used.
     *
     * @param args New environment key-value map of properties.
     *
     * @since 0.7
     */
    void setEnvironment(Map<String, ?> args) {
        if (task) {
            taskOnlyEnvironment = true
        }
        this.env.value(EMPTY_MAP)
        this.env.putAll((Map<String, Object>) args)
    }

    /** Environment for running the exe
     *
     * <p> Calling this will resolve all lazy-values in the variable map.
     *
     * @return Map of environmental variables that will be passed.
     *
     * @since 0.7
     */
    Map<String, String> getEnvironment() {
        this.envProvider.get()
    }

    @Override
    Provider<Map<String, String>> getEnvironmentProvider() {
        this.envProvider
    }

    /** Add environmental variables to be passed to the exe.
     *
     * @param args Environmental variable key-value map.
     *
     * @since 0.7
     */
    void environment(Map<String, ?> args) {
        if (!this.env.present) {
            this.env.value(EMPTY_MAP)
        }
        this.env.putAll((Map<String, Object>) args)
    }

    /**
     * Adds the system path to the execution environment.
     *
     * @since 0.7
     */
    void useSystemPath() {
        final systemPath = configCacheSafeOperations.providerTools().environmentVariable(OS.pathVar)
        appendPath(systemPath)
    }

    /** Add search to system path.
     *
     * In the case of a task this will be appended to both the task and project extension's version
     * of the system path.
     *
     * @param postfix Provider of a path item that can be appended to the current system path.
     *
     * @since 0.9.2
     */
    void appendPath(Provider<String> postfix) {
        if (task && taskOnlyEnvironment) {
            appendPaths.add(postfix)
        } else if (task) {
            appendPaths.add(postfix)
            projectExtension.appendPath(postfix)
        } else {
            appendPaths.add(postfix)
        }
//        Object current = this.env[OS.pathVar]
//        if (task && taskOnlyEnvironment) {
//            if (current == null) {
//                this.env[OS.pathVar] = postfix
//            } else {
//                this.env[OS.pathVar] = postfix.map { String s -> "${stringize(current)}${OS.pathSeparator}${s}" }
//            }
//        } else if (task) {
//            Provider<String> combinator
//            final parent = projExt
//            if (current == null) {
//                combinator = postfix.map({ String s ->
//                    "${stringize(parent.env[OS.pathVar])}${OS.pathSeparator}${s}"
//                } as Transformer<String, String>)
//            } else {
//                combinator = postfix.map({ String s ->
//                    "${stringize(current)}${OS.pathSeparator}${s}"
//                } as Transformer<String, String>)
//            }
//            this.env[OS.pathVar] = combinator
//        } else {
//            if (current == null) {
//                this.env[OS.pathVar] = postfix
//            } else {
//                this.env[OS.pathVar] = postfix.map { String s -> "${stringize(current)}${OS.pathSeparator}${s}" }
//            }
//        }
    }

    /**
     * Add search to system path
     *
     * <p>
     * In the case of a task this will be prefixed to both the task and project extension's version
     * of the system path.
     * </p>
     *
     * <p>
     *     Prefixes are in reversed other - the last prefix added will be the first in the path list.
     * </p>
     *
     * @param prefix Provider of a path item that can be prefixed to the current system path.
     *
     * @since 0.9.2
     */
    void prefixPath(Provider<String> prefix) {
        if (task && taskOnlyEnvironment) {
            prefixPaths.add(prefix)
        } else if (task) {
            prefixPaths.add(prefix)
            projectExtension.prefixPath(prefix)
        } else {
            prefixPaths.add(prefix)
        }

//        Object current = this.env[OS.pathVar]
//        if (task && taskOnlyEnvironment) {
//            if (current == null) {
//                this.env[OS.pathVar] = prefix
//            } else {
//                this.env[OS.pathVar] = prefix.map { String s -> "${s}${OS.pathSeparator}${stringize(current)}" }
//            }
//        } else if (task) {
//            Provider<String> combinator
//            T parent = projExt
//            if (current == null) {
//                combinator = prefix.map({ String s ->
//                    "${s}${OS.pathSeparator}${stringize(parent.env[OS.pathVar])}"
//                } as Transformer<String, String>)
//            } else {
//                combinator = prefix.map({ String s ->
//                    "${s}${OS.pathSeparator}${stringize(current)}"
//                } as Transformer<String, String>)
//            }
//            this.env[OS.pathVar] = combinator
//        } else {
//            if (current == null) {
//                this.env[OS.pathVar] = prefix
//            } else {
//                this.env[OS.pathVar] = prefix.map { String s -> "${s}${OS.pathSeparator}${stringize(current)}" }
//            }
//        }
    }

    /** Constructs a new extension which is attached to the provided project.
     *
     * @param project Project this extension is associated with.
     */
    protected BaseNodeJSExtension(Project project) {
        super(ProjectOperations.find(project))
        this.configCacheSafeOperations = ConfigCacheSafeOperations.from(project)

        final fsOperations = configCacheSafeOperations.fsOperations()
        final verProps = fsOperations.loadPropertiesFromResource(
            'META-INF/javascript-gradle/node/versions.properties'
        )
        this.prefixPaths = project.objects.listProperty(String)
        this.prefixPaths.value(EMPTY_LIST)
        this.appendPaths = project.objects.listProperty(String)
        this.appendPaths.value(EMPTY_LIST)
        executableByVersion(verProps['node'])
        this.env = configCacheSafeOperations.providerTools().mapProperty(String, Object)
        this.env.putAll(NodeJSExecutor.defaultEnvironment)
        this.npmCliProvider = linkNpmCli()
        this.toolDownloader = new Downloader(configCacheSafeOperations)
        this.locateNpm = createNpmLocator(project.extensions, true)

        prefixPath(executable.map { File it ->
            it.parentFile.canonicalPath
        })

        this.envProvider = configCacheSafeOperations.stringTools()
            .provideValues(this.env)
            .zip(this.prefixPaths.orElse(EMPTY_LIST), PREFIX_PATH_ADJUSTER)
            .zip(this.appendPaths.orElse(EMPTY_LIST), APPEND_PATH_ADJUSTER)
    }

    /**
     * Constructs a new extension which is attached to the provided task.
     *
     * @param project Project this extension is associated with.
     * @param alternativeProjectExt Alternative extension to use, rather than the default project extension
     */
    protected BaseNodeJSExtension(Task task, T alternativeProjectExt) {
        super(task, ProjectOperations.find(task.project), alternativeProjectExt)
        this.configCacheSafeOperations = ConfigCacheSafeOperations.from(task.project)
        this.prefixPaths = task.project.objects.listProperty(String)
        this.appendPaths = task.project.objects.listProperty(String)
        this.taskOnlyEnvironmentProvider = task.project.provider { -> owner.taskOnlyEnvironment }
        this.env = configCacheSafeOperations.providerTools().mapProperty(String, Object)
        this.npmCliProvider = linkNpmCli()
        this.toolDownloader = new Downloader(configCacheSafeOperations)
        this.locateNpm = createNpmLocator(task.extensions, false)

        final filterProvider = projectExtension.environmentProvider
            .zip(taskOnlyEnvironmentProvider) { Map<String, String> projectEnv, Boolean taskOnly ->
                taskOnly ? EMPTY_MAP : projectEnv
            }

        this.envProvider = configCacheSafeOperations.stringTools().provideValues(this.env)
            .zip(filterProvider, MERGE_TASK_MAP)
            .zip(this.prefixPaths.orElse(EMPTY_LIST), PREFIX_PATH_ADJUSTER)
            .zip(this.appendPaths.orElse(EMPTY_LIST), APPEND_PATH_ADJUSTER)
            .orElse(projectExtension.environmentProvider)
    }

    /** Constructs a new extension which is attached to the provided task.
     *
     * This is meant for other plugins that want to provide their own NodeJSExtension implementation
     * @param project Project this extension is associated with.
     * @param projectExtName Name of the extension that is attached to the project.
     *
     * @since 0.4
     */
    protected BaseNodeJSExtension(Task task, final String projectExtName) {
        super(
            task,
            ProjectOperations.find(task.project),
            (T) task.project.extensions.getByName(projectExtName)
        )
        this.configCacheSafeOperations = ConfigCacheSafeOperations.from(task.project)
        this.prefixPaths = task.project.objects.listProperty(String)
        this.appendPaths = task.project.objects.listProperty(String)
        this.taskOnlyEnvironmentProvider = task.project.provider { -> owner.taskOnlyEnvironment }
        this.env = configCacheSafeOperations.providerTools().mapProperty(String, Object)
        this.npmCliProvider = linkNpmCli()
        this.toolDownloader = new Downloader(configCacheSafeOperations)

        final filterProvider = projectExtension.environmentProvider
            .zip(taskOnlyEnvironmentProvider) { Map<String, String> projectEnv, Boolean taskOnly ->
                taskOnly ? EMPTY_MAP : projectEnv
            }

        this.envProvider = configCacheSafeOperations.stringTools().provideValues(this.env)
            .zip(filterProvider, MERGE_TASK_MAP)
            .zip(this.prefixPaths.orElse(EMPTY_LIST), PREFIX_PATH_ADJUSTER)
            .zip(this.appendPaths.orElse(EMPTY_LIST), APPEND_PATH_ADJUSTER)
            .orElse(projectExtension.environmentProvider)
    }

    /** Constructs a new extension which is attached to the provided task.
     *
     * This is meant for other plugins that want to provide their own NodeJSExtension implementation
     * @param project Project this extension is associated with.
     * @param extClass A function object that will return an extension that is attached to the project.
     *
     * @since 3.0
     */
    protected BaseNodeJSExtension(Task task, Callable<T> extClass) {
        super(
            task,
            ProjectOperations.find(task.project),
            extClass.call()
        )
        this.configCacheSafeOperations = ConfigCacheSafeOperations.from(task.project)
        this.prefixPaths = task.project.objects.listProperty(String)
        this.appendPaths = task.project.objects.listProperty(String)
        this.taskOnlyEnvironmentProvider = task.project.provider { -> owner.taskOnlyEnvironment }
        this.env = configCacheSafeOperations.providerTools().mapProperty(String, Object)
        this.npmCliProvider = linkNpmCli()
        this.toolDownloader = new Downloader(configCacheSafeOperations)

        final filterProvider = projectExtension.environmentProvider
            .zip(taskOnlyEnvironmentProvider) { Map<String, String> projectEnv, Boolean taskOnly ->
                taskOnly ? EMPTY_MAP : projectEnv
            }

        this.envProvider = configCacheSafeOperations.stringTools().provideValues(this.env)
            .zip(filterProvider, MERGE_TASK_MAP)
            .zip(this.prefixPaths.orElse(EMPTY_LIST), PREFIX_PATH_ADJUSTER)
            .zip(this.appendPaths.orElse(EMPTY_LIST), APPEND_PATH_ADJUSTER)
            .orElse(projectExtension.environmentProvider)
    }

    /**
     * Runs the executable and returns the version.
     *
     * See {@link org.ysb33r.grolifant5.internal.core.executable.ExecUtils#parseVersionFromOutput}
     *   as a helper to implement this method.
     *
     * @return Version string.
     * @throws org.ysb33r.grolifant5.api.errors.ConfigurationException if configuration is not correct or version
     *   could not be determined.
     *
     * @since 0.10
     */
    @Override
    protected String runExecutableAndReturnVersion() throws ConfigurationException {
        ExecUtils.parseVersionFromOutput(
            projectOperations.execTools,
            ['-v'],
            executable.get(),
            { String output ->
                output.readLines()[0].replaceFirst('v', '')
            }) { ExecSpec spec ->
            spec.environment(org.ysb33r.gradle.nodejs.utils.NodeJSExecutor.defaultEnvironment)
        }
    }

    /**
     * Gets the downloader implementation.
     *
     * Can throw an exception if downloading is not supported.
     *
     * @return A downloader that can be used to retrieve an executable.
     *
     * @since 0.10
     */
    @Override
    protected ExecutableDownloader getDownloader() {
        toolDownloader.executableDownloader
    }

    protected final Callable<NpmExtensionMethods> locateNpm

    private Provider<File> linkNpmCli() {
        executable.map { File exe ->
            File root
            if (OS.windows) {
                root = exe.parentFile
            } else {
                root = new File(exe.parentFile.parentFile, 'lib')
            }
            new File(root, 'node_modules/npm/bin/npm-cli.js')
        }
    }

    private static Callable<NpmExtensionMethods> createNpmLocator(ExtensionContainer exts, boolean nullOnException) {
        new Callable<NpmExtensionMethods>() {
            @Override
            NpmExtensionMethods call() throws Exception {
                try {
                    NpmExtensionMethods nem = exts.getByType(NpmExtension)
                    nem
                } catch (UnknownDomainObjectException e) {
                    if (nullOnException) {
                        null
                    } else {
                        throw e
                    }
                }
            }
        }
    }

    private boolean taskOnlyEnvironment = false
    private final Provider<Boolean> taskOnlyEnvironmentProvider
    private final Provider<Map<String, String>> envProvider
    private final MapProperty<String, Object> env
    private final Provider<File> npmCliProvider
    private final Downloader toolDownloader
    private final ListProperty<String> prefixPaths
    private final ListProperty<String> appendPaths
    private static final OperatingSystem OS = OperatingSystem.current()

    private static final BiFunction<Map<String, String>, List<String>, Map<String, String>> PREFIX_PATH_ADJUSTER = {
        Map<String, String> envVars, List<String> paths ->
            final pathStr = paths.reverse().join(OS.pathSeparator)
            if (pathStr) {
                Map<String, String> replaced = [:]
                replaced.putAll(envVars)
                replaced.put(
                    OS.pathVar,
                    envVars[OS.pathVar] ? "${pathStr}${OS.pathSeparator}${envVars[OS.pathVar]}".toString() : pathStr
                )
                replaced
            } else {
                envVars
            }
    }

    private static final BiFunction<Map<String, String>, List<String>, Map<String, String>> APPEND_PATH_ADJUSTER = {
        Map<String, String> envVars, List<String> paths ->
            final pathStr = paths.join(OS.pathSeparator)
            if (pathStr) {
                Map<String, String> replaced = [:]
                replaced.putAll(envVars)
                replaced.put(
                    OS.pathVar,
                    envVars[OS.pathVar] ? "${envVars[OS.pathVar]}${OS.pathSeparator}${pathStr}".toString() : pathStr
                )
                replaced
            } else {
                envVars
            }
    }

    private static final BiFunction<Map<String, String>, Map<String, String>, Map<String, String>> MERGE_TASK_MAP = {
        Map<String, String> taskEnv, Map<String, String> projEnv ->
            if (projEnv.isEmpty()) {
                taskEnv
            } else {
                Map<String, String> merged = [:]
                merged.putAll(projEnv)
                merged.putAll(taskEnv)
                merged
            }
    }
}
