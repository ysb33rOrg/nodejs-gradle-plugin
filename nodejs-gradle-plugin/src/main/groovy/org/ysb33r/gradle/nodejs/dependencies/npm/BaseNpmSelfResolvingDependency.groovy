/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.nodejs.dependencies.npm

import groovy.transform.CompileStatic
import groovy.transform.PackageScope
import groovy.util.logging.Slf4j
import org.gradle.api.Task
import org.gradle.api.artifacts.Dependency
import org.gradle.api.artifacts.FileCollectionDependency
import org.gradle.api.artifacts.component.ComponentIdentifier
import org.gradle.api.file.FileCollection
import org.gradle.api.file.FileTree
import org.gradle.api.internal.artifacts.dependencies.SelfResolvingDependencyInternal
import org.gradle.api.tasks.TaskDependency
import org.ysb33r.gradle.nodejs.NodeJSConfigCacheSafeOperations
import org.ysb33r.gradle.nodejs.NodeJSExtensionMethods
import org.ysb33r.gradle.nodejs.NpmConfigCacheSafeOperations
import org.ysb33r.gradle.nodejs.NpmDependency
import org.ysb33r.gradle.nodejs.NpmExtensionMethods
import org.ysb33r.gradle.nodejs.internal.npm.NpmComponentIdentifier
import org.ysb33r.gradle.nodejs.utils.npm.NpmExecutor
import org.ysb33r.grolifant5.api.core.ConfigCacheSafeOperations
import org.ysb33r.grolifant5.api.core.OperatingSystem
import org.ysb33r.grolifant5.api.core.ProjectOperations

import javax.annotation.Nullable

/**
 * An NPM dependency that can resolve itself.
 *
 * @since 2.2
 */
@CompileStatic
@Slf4j
class BaseNpmSelfResolvingDependency
    extends NpmDependency
    implements FileCollectionDependency, SelfResolvingDependencyInternal {

    public static final String NPM_MAVEN_GROUP_PREFIX = '__npm__'
    public static final String NPM_MAVEN_GROUP_NO_SCOPE = "${NPM_MAVEN_GROUP_PREFIX}.__global__"

    /**
     * Constructs the basic of an NPM self-resolving dependency.
     *
     * @param ccso Configuration cache-safe operations
     * @param nodeJSExtension
     * @param npmExtension
     * @param props
     */
    BaseNpmSelfResolvingDependency(
        ConfigCacheSafeOperations ccso,
        NodeJSConfigCacheSafeOperations nodeJSExtension,
        NpmConfigCacheSafeOperations npmExtension,
        final Map<String, Object> props
    ) {
        super(onlyNpmDependencyKeys(props))
        this.ccso = ccso
        this.nodejs = NodeJSConfigCacheSafeOperations.from(nodeJSExtension)
        this.npm = NpmConfigCacheSafeOperations.from(npmExtension)
        this.npmExecutor = new NpmExecutor(ccso, this.nodejs, this.npm)
        this.installArgs.addAll(extractInstallArgs(props))
        this.path = props[PATH]
    }

    @Deprecated
    BaseNpmSelfResolvingDependency(
        ProjectOperations projectOperations,
        NodeJSExtensionMethods nodeJSExtension,
        NpmExtensionMethods npmExtension,
        final Map<String, Object> props
    ) {
        super(onlyNpmDependencyKeys(props))
        this.ccso = ConfigCacheSafeOperations.from(projectOperations)
        this.nodejs = NodeJSConfigCacheSafeOperations.from(nodeJSExtension)
        this.npm = NpmConfigCacheSafeOperations.from(npmExtension)
        this.npmExecutor = new NpmExecutor(
            ConfigCacheSafeOperations.from(projectOperations),
            nodeJSExtension,
            npmExtension
        )
        this.installArgs.addAll(extractInstallArgs(props))
        this.path = props[PATH]
    }

    @Override
    String getGroup() {
        scope ? "${NPM_MAVEN_GROUP_PREFIX}.${scope}" : NPM_MAVEN_GROUP_NO_SCOPE
    }

    @Override
    String getName() {
        this.packageName
    }

    @Override
    String getVersion() {
        this.tagName == '+' ? 'latest' : this.tagName
    }

    @Override
    boolean contentEquals(Dependency dependency) {
        dependency instanceof BaseNpmSelfResolvingDependency &&
            ((BaseNpmSelfResolvingDependency) dependency).packageName == packageName &&
            ((BaseNpmSelfResolvingDependency) dependency).tagName == tagName &&
            ((BaseNpmSelfResolvingDependency) dependency).scope == scope
    }

    @Override
    @SuppressWarnings('UnnecessaryCast')
    Dependency copy() {
        Map<String, Object> pkg = [name: packageName, tag: tagName, type: installGroup] as Map<String, Object>
        if (scope) {
            pkg.put('scope', scope)
        }
        if (!installArgs.empty) {
            pkg.put(INSTALL_ARGS, installArgs)
        }
        if (path) {
            pkg.put(PATH, path)
        }
        new BaseNpmSelfResolvingDependency(
            ccso,
            nodejs,
            npm,
            (Map<String, Object>) pkg
        )
    }

    @Override
    String getReason() {
        this.reason
    }

    @Override
    void because(@Nullable String s) {
        this.reason = s
    }

    @Override
    Set<File> resolve() {
        Map<String, Object> env = [:]

        if (path) {
            env[OperatingSystem.current().pathVar] = path
        }

        npmExecutor.installNpmPackage(this, installGroup, installArgs, env).files
    }

    @Override
    Set<File> resolve(boolean b) {
        log.warn('Ignoring transitive parameter for NPM')
        resolve()
    }

    final TaskDependency buildDependencies = NO_TASK_DEPENDENCIES

    @Override
    FileCollection getFiles() {
        FileTree fileTree = ccso.fsOperations().fileTree(npmExecutor.getPackageInstallationFolder(this))

        if (fileTree.empty) {
            resolve()
        }

        fileTree
    }

    @Override
    ComponentIdentifier getTargetComponentId() {
        new NpmComponentIdentifier("NPM: ${this}")
    }

    @Override
    String toString() {
        "${group}:${name}:${version}"
    }

    @PackageScope
    String getPath() { this.path }

    @PackageScope
    List<String> getInstallArgs() { this.installArgs }

    private static class NoTaskDependencies implements TaskDependency {
        static final Set EMPTY_SET = []

        @Override
        Set<? extends Task> getDependencies(Task task) {
            EMPTY_SET
        }
    }

    private List<String> extractInstallArgs(Map<String, Object> userMap) {
        ccso.stringTools().stringize((userMap[INSTALL_ARGS] ?: []) as List)
    }

    private static final NoTaskDependencies NO_TASK_DEPENDENCIES = new NoTaskDependencies()
    private final static String PATH = 'path'
    private final static String INSTALL_ARGS = 'install-args'
    private String reason
    private final ConfigCacheSafeOperations ccso
    private final NodeJSConfigCacheSafeOperations nodejs
    private final NpmConfigCacheSafeOperations npm
    private final NpmExecutor npmExecutor
    private final List<String> installArgs = []
    private final String path
}
