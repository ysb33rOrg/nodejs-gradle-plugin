/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.nodejs.internal.npm

import groovy.transform.CompileStatic
import org.gradle.api.provider.Provider
import org.ysb33r.gradle.nodejs.NodeJSExtension
import org.ysb33r.gradle.nodejs.NpmExtension
import org.ysb33r.gradle.nodejs.SimpleNpmPackageDescriptor
import org.ysb33r.gradle.nodejs.pkgwrapper.PackageEntryPoint
import org.ysb33r.gradle.nodejs.pkgwrapper.PackageInstallGroup
import org.ysb33r.grolifant5.api.core.ConfigCacheSafeOperations
import org.ysb33r.grolifant5.api.core.ProjectOperations

/**
 * A base class for creating resolvers for specific NPM packages.
 *
 * <p> This class is meant to be used when creating an ecosystem for specific broad-use
 * NPM-based tools such as Gulp.
 *
 * @since 0.2
 */
@CompileStatic
class TagBasedNpmPackageResolver {

    /**
     *
     * @param scope Resolve scope. Can be {@code null}.
     * @param packageName The name of the package that will be resolved.
     * @param projectOperations Project this is associated with
     * @param nodeExtension Node extension that should be used for resolving Node questions.
     * @param npmExtension NPM extension that shgoul dbe used for resolving NPM questions.
     * @param installGroupLocator Ability to obtain the installation group for a package when package needs to
     *   be resolved.
     * @param entryPointLocator Ability to obtain the entry point for a package when package needs to be resolved.
     *
     * @since 4.0
     */
    @SuppressWarnings('ParameterCount')
    TagBasedNpmPackageResolver(
        final String scope,
        final String packageName,
        ConfigCacheSafeOperations ccso,
        NodeJSExtension nodeExtension,
        NpmExtension npmExtension,
        PackageInstallGroup installGroupLocator,
        PackageEntryPoint entryPointLocator
    ) {
        this.scope = scope
        this.packageName = packageName
        this.groupLocator = installGroupLocator
        this.entryPointLocator = entryPointLocator
        this.npmPackageResolver = new NpmPackageResolver(
            ccso,
            nodeExtension,
            npmExtension
        )
        this.ccso = ccso
    }
    /**
     *
     * @param scope Resolve scope. Can be {@code null}.
     * @param packageName The name of the package that will be resolved.
     * @param projectOperations Project this is associated with
     * @param nodeExtension Node extension that should be used for resolving Node questions.
     * @param npmExtension NPM extension that shgoul dbe used for resolving NPM questions.
     * @param installGroupLocator Ability to obtain the installation group for a package when package needs to
     *   be resolved.
     * @param entryPointLocator Ability to obtain the entry point for a package when package needs to be resolved.
     *
     * @deprecated
     */
    @SuppressWarnings('ParameterCount')
    @Deprecated
    TagBasedNpmPackageResolver(
        final String scope,
        final String packageName,
        ProjectOperations projectOperations,
        NodeJSExtension nodeExtension,
        NpmExtension npmExtension,
        PackageInstallGroup installGroupLocator,
        PackageEntryPoint entryPointLocator
    ) {
        this(
            scope,
            packageName,
            ConfigCacheSafeOperations.from(projectOperations),
            nodeExtension,
            npmExtension,
            installGroupLocator,
            entryPointLocator
        )
    }

    Provider<File> resolverFromVersion(Object version) {
        ccso.stringTools().provideString(version).map {
            final descriptor = new SimpleNpmPackageDescriptor(
                scope,
                packageName,
                it
            )
            npmPackageResolver.resolvesWithEntryPoint(descriptor, entryPointLocator.entryPoint, groupLocator.group)
        }
    }

    Provider<File> resolverFromPath(Object path) {
        ccso.fsOperations().provideFile(path)
    }

    private final String scope
    private final String packageName
    private final PackageInstallGroup groupLocator
    private final PackageEntryPoint entryPointLocator
    private final NpmPackageResolver npmPackageResolver
    private final ConfigCacheSafeOperations ccso
}
