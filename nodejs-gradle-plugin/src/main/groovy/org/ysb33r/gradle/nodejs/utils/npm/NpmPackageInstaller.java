/**
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.nodejs.utils.npm;

import org.gradle.api.file.FileTree;

/**
 * Abstract the installaiotn of NPM packages.
 *
 * @author Schalk W. Cronjé
 *
 * @since 4.0
 */
public interface NpmPackageInstaller {
    /**
     * Executes a package installation and return the file tree
     * @return Installed file tree
     */
    FileTree run();
}
