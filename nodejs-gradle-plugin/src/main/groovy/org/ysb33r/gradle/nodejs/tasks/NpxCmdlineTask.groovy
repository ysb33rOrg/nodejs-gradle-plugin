/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.nodejs.tasks

import groovy.transform.CompileStatic

/**
 * Providers a task for running npx with arguments.
 * By default It will be executed within the directory which is configured as the NPM home directory.
 *
 * @since 0.9.0
 */
@CompileStatic
class NpxCmdlineTask extends AbstractCmdlineTask {
    NpxCmdlineTask() {
        super()
        description = 'Runs npx with --args'
        commandProvider = nodejs.executable.map { it.absolutePath }

        final npmConfig = npm.localConfigProvider.zip(npm.globalConfigProvider) { l, g ->
            [
                npm_config_userconfig  : l.absolutePath,
                npm_config_globalconfig: g.absolutePath
            ]
        }

        environmentProvider = nodejs.environmentProvider.zip(npmConfig) { env, npmEnv ->
            env + npmEnv
        }

        addArgs(npm.npxCliJsProvider.map { File it -> it.absolutePath })
    }
}
