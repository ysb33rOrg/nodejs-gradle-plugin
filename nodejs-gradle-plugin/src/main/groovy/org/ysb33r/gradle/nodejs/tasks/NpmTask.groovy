/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.nodejs.tasks

import groovy.transform.CompileStatic
import org.gradle.api.tasks.Internal
import org.ysb33r.gradle.nodejs.NodeJSExtension
import org.ysb33r.gradle.nodejs.NpmExecSpec
import org.ysb33r.gradle.nodejs.NpmExtension
import org.ysb33r.grolifant5.api.core.runnable.AbstractCommandExecTask

import static org.ysb33r.gradle.nodejs.utils.npm.NpmExecutor.createNpmExecSpecWithDefaultEnvironment

/**
 * Ability to execute any NPM command with parameters.
 *
 * <p> The task contains an NPM extension, which by default is setup to look at the
 * global NPM extension. It allows overriding on a per-task basis of NPM configuration.
 *
 * @since 0.1
 */
@CompileStatic
class NpmTask extends AbstractCommandExecTask<NpmExecSpec> {
    NpmTask() {
        super()
        final npmExtension = (NpmExtension) (extensions.create(NpmExtension.NAME, NpmExtension, this))
        final nodeExtension = (NodeJSExtension) (extensions.create(NodeJSExtension.NAME, NodeJSExtension, this))
        this.execSpec = createNpmExecSpecWithDefaultEnvironment(this, nodeExtension, npmExtension)
    }

    /**
     * Access to the underlying {@link NpmExecSpec}.
     *
     * @return NPM execution specification.
     *
     * @since 2.0
     */
    @Override
    protected NpmExecSpec getNativeExecSpec() {
        this.execSpec
    }

    @Internal
    protected NpmExtension getNpm() {
        extensions.getByType(NpmExtension)
    }

    @Internal
    protected NodeJSExtension getNodejs() {
        extensions.getByType(NodeJSExtension)
    }

    private final NpmExecSpec execSpec
}
