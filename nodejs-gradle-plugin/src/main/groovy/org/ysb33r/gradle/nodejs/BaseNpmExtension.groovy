/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.nodejs

import groovy.transform.CompileStatic
import groovy.util.logging.Slf4j
import org.gradle.api.Project
import org.gradle.api.Task
import org.gradle.api.provider.Property
import org.gradle.api.provider.Provider
import org.gradle.process.ExecSpec
import org.ysb33r.gradle.nodejs.internal.npm.NpmInstaller
import org.ysb33r.gradle.nodejs.internal.npm.NpmrcProviders
import org.ysb33r.gradle.nodejs.utils.NodeJSExecutor
import org.ysb33r.grolifant5.api.core.ConfigCacheSafeOperations
import org.ysb33r.grolifant5.api.core.ProjectOperations
import org.ysb33r.grolifant5.api.core.downloader.ExecutableDownloader
import org.ysb33r.grolifant5.api.core.runnable.AbstractToolExtension
import org.ysb33r.grolifant5.api.core.runnable.ProvisionedExecMethods
import org.ysb33r.grolifant5.api.errors.ConfigurationException
import org.ysb33r.grolifant5.internal.core.executable.ExecUtils

/** Set up global config or task-based config for NPM.
 *
 * @since 2.1
 */
@CompileStatic
@Slf4j
class BaseNpmExtension<T extends BaseNodeJSExtension<T>, S extends BaseNpmExtension<T, S>>
    extends AbstractToolExtension<S>
    implements ProvisionedExecMethods<NpmExecSpec>, NpmExtensionMethods {

    final ConfigCacheSafeOperations configCacheSafeOperations

    /**
     * Use the NPM that is bundled with Node.
     *
     * This is the default and should raely be required.
     *
     * @since 0.12
     */
    void executableIsBundled() {
        executableByPath(nodeJsProjectExtension.npmCliJsProvider)
    }

    /**
     * The location of NPX.
     *
     * @return A provider to the location of NPX
     *
     * @since 0.10
     */
    Provider<File> getNpxCliJsProvider() {
        this.npxProvider
    }

    /**
     * Create execution specification.
     *
     * @return Execution specification.
     *
     * @since 0.10
     */
    @Override
    NpmExecSpec createExecSpec() {
        new NpmExecSpec(nodeJsProjectExtension.executable, executable, configCacheSafeOperations)
    }

    /**
     * Location & name of global NPM config file.
     *
     * When this extension is attached to a project, the default location is set to
     * {@code "${project.gradle.gradleUserHomeDir}/npmrc"}
     *
     * @return {@link java.io.File} object pointing to global NPM config
     *
     * @deprecated Use {@link #getGlobalConfigProvider} instead.
     */
    @Deprecated
    File getGlobalConfig() {
        globalConfigProvider.get()
    }

    /**
     * Location & name of global NPM config file.
     *
     * When this extension is attached to a project, the default location is set to
     * {@code "${project.gradle.gradleUserHomeDir}/npmrc"}
     *
     * @return Provider to the global NPM config location for Gradle.
     *
     * @since 3.0
     */
    @Override
    Provider<File> getGlobalConfigProvider() {
        this.globalConfigLocation
    }

    /** Set global config file.
     *
     * @param path Anything that can be converted using {@code project.file}.
     */
    void setGlobalConfig(Object path) {
        projectOperations.fsOperations.updateFileProperty(this.globalConfigLocation, path)
    }

    /** Set global config file.
     *
     * @param path Anything that can be converted using {@code project.file}.
     *
     * @deprecated Use {@link #setGlobalConfig}
     */
    @Deprecated
    void globalConfig(Object path) {
        globalConfig = path
    }

    /**
     * Location & name of local NPM config file.
     *
     * When this extension is attached to a project, the default location is set to
     * {@code "${project.rootProject.projectDir}/npmrc"}
     *
     * @return {@link java.io.File} object pointing to local NPM config
     *
     * @deprecated Use {@link #getLocalConfigProvider} instead
     */
    @Deprecated
    File getLocalConfig() {
        localConfigLocation.get()
    }

    /**
     * Location & name of local NPM config file.
     *
     * When this extension is attached to a project, the default location is set to
     * {@code "${project.rootProject.projectDir}/npmrc"}
     *
     * @return Provider to local NPM config
     */
    @Override
    Provider<File> getLocalConfigProvider() {
        this.localConfigLocation
    }

    /** Set local config file.
     *
     * @param path Anything that can be converted using {@code project.file}.
     */
    void setLocalConfig(Object path) {
        projectOperations.fsOperations.updateFileProperty(this.localConfigLocation, path)
    }

    /**
     * Location of {@code .npmrc} for a project.
     *
     * @return Project config level.
     */
    Provider<File> getProjectConfig() {
        npmrcProviders.project
    }

    @Override
    Provider<File> getProjectConfigProvider() {
        npmrcProviders.project
    }

/** Set local config file.
 *
 * @param path Anything that can be converted using {@code project.file}.
 */
    void localConfig(Object path) {
        localConfig = path
    }

    /** The NPM home directory - the parent directory of {@code node_modules},
     *
     * @return Parent directory of {@code node_modules}. Never null if the extension is tied to a project,
     * in which case it defaults to {@code project.projectDir}.
     *
     * @deprecated Use {@link #getHomeDirectoryProvider} instead.
     */
    @Deprecated
    File getHomeDirectory() {
        homeDirectoryProvider.get()
    }

    /** The NPM home directory - the parent directory of {@code node_modules},
     *
     * @return Parent directory of {@code node_modules}. Never null if the extension is tied to a project,
     * in which case it defaults to {@code project.projectDir}.
     *
     * @since 0.8.0
     */
    Provider<File> getHomeDirectoryProvider() {
        this.homeDirectoryProperty
    }

    /**
     * Provides the version as specified in {@code package.json}.
     *
     * @return Version provider.
     *
     * @since 2.0
     */
    Provider<String> getVersionProvider() {
        homeDirectoryProvider.map {
            try {
                PackageJson.loadVersionFromPackageJson(new File(it, 'package.json'))
            } catch (FileNotFoundException e) {
                'undefined'
            }
        }
    }

    /** Sets the home directory.
     *
     * @param homeDir A directory in which {@code node_modules} will be created as a child folder.
     *   Anything that can be resolved with {@code project.file} is acceptable
     */
    void setHomeDirectory(Object homeDir) {
        configCacheSafeOperations.fsOperations().updateFileProperty(this.homeDirectoryProperty, homeDir)
    }

    /**
     * Sets the home directory.
     *
     * @param homeDir A directory in which {@code node_modules} will be created as a child folder.
     *   Anything that can be resolved with {@code project.file} is acceptable
     *
     * @deprecated Use {@link #setHomeDirectory} instead
     */
    @Deprecated
    void homeDirectory(Object homeDir) {
        homeDirectory = homeDir
    }

    /**
     * Tries to determine the location where global config will typically be places.
     *
     * @return A provider to a system location.
     *
     * @since 0.12
     */
    Provider<File> useGlobalSystemLocation() {
        npmrcProviders.system
    }

    /**
     * Sets it to use the equivalent of {@code getGradleHomeDir()/.npmrc}.
     *
     * This allows Gradle projects to be custom configured on local system without being affected by any system-wide
     * node installations.
     *
     * @return Location of a global configuration file for NPM that is located inside the Gradle home directory.
     *
     * @since 0.12
     */
    Provider<File> useGlobalGradleLocation() {
        npmrcProviders.gradle
    }

    /**
     * Use to indicate that userconfig should be retrieved from the user's home directory.
     *
     * Typically this is {@code ~/.npmrc} or {@code %APPDATA\npm\.npmrc}
     *
     * @return Location for a userconfig {@code .npmrc}
     *
     * @since 0.12
     */
    Provider<File> useLocalUserProfileLocation() {
        npmrcProviders.user
    }

    /**
     * Use to indicate that userconfig should be retrieved from the NPM home directory as configured by
     * {@link #getHomeDirectory()}.
     *
     * @return Location for a userconfig {@code .npmrc}
     *
     * @since 0.12
     */
    Provider<File> useLocalProjectLocation() {
        projectConfig
    }

    /**
     * Use the version specified in {@code package.json} as the version of the project.
     *
     * If {@code package.json} does not exist, then {@code undefined} is used as the version.
     */
    void usePackageJsonVersionAsProjectVersion() {
        configCacheSafeOperations.projectTools().versionProvider = versionProvider
    }

    /** Adds the extension to the project.
     *
     * @param project Project to link to.
     * @param nodejs NodeJS extension to use.
     *
     * @since 0.10
     */
    protected BaseNpmExtension(Project project, T nodejs) {
        super(ProjectOperations.find(project))
        this.configCacheSafeOperations = ConfigCacheSafeOperations.from(project)
        this.nodeJsProjectExtension = nodejs
        this.homeDirectoryProperty = project.objects.property(File)
        this.homeDirectoryProperty.set(project.projectDir)
        executableByPath(nodeJsProjectExtension.npmCliJsProvider)
        this.npxProvider = executable.map { File it -> new File(it.parentFile, NPX_FILENAME) }

        this.npmrcProviders = new NpmrcProviders(
            projectOperations,
            project.gradle.gradleUserHomeDir,
            this.homeDirectoryProperty
        )

        this.globalConfigLocation = project.objects.property(File)
        projectOperations.fsOperations.updateFileProperty(this.globalConfigLocation, useGlobalGradleLocation())

        this.localConfigLocation = project.objects.property(File)
        projectOperations.fsOperations.updateFileProperty(this.localConfigLocation, useLocalProjectLocation())

        this.npmInstaller = new NpmInstaller(
            ConfigCacheSafeOperations.from(projectOperations),
            this.nodeJsProjectExtension,
            this
        )
    }

    /** Adds the extension to a {@link org.ysb33r.gradle.nodejs.tasks.NpmTask} task.
     *
     * <p> Links the executable resolving to the global instance, but allows
     * it to be overridden on a per-task basis.
     *
     * @param task Task to be extended.
     * @param nodejs Alternative NodeJS extension to use as project extension.
     * @param altNpmExt Alternative Npm extension to use as project extension.
     *
     * @since 0.10
     */
    protected BaseNpmExtension(Task task, T nodejs, S altNpmExt) {
        super(
            task,
            ProjectOperations.find(task.project),
            altNpmExt
        )
        this.configCacheSafeOperations = ConfigCacheSafeOperations.from(task.project)
        this.nodeJsProjectExtension = nodejs
        this.homeDirectoryProperty = task.project.objects.property(File)
        this.homeDirectoryProperty.set(npmExtension.homeDirectoryProvider)
        this.npxProvider = executable.map { File it -> new File(it.parentFile, NPX_FILENAME) }

        this.npmrcProviders = new NpmrcProviders(
            projectOperations,
            task.project.gradle.gradleUserHomeDir,
            this.homeDirectoryProperty
        )

        this.npmInstaller = new NpmInstaller(configCacheSafeOperations, nodejs, this)

        this.globalConfigLocation = task.project.objects.property(File)
        projectOperations.fsOperations.updateFileProperty(this.globalConfigLocation, npmExtension.globalConfigProvider)

        this.localConfigLocation = task.project.objects.property(File)
        projectOperations.fsOperations.updateFileProperty(this.localConfigLocation, npmExtension.localConfigProvider)
    }

    /**
     * Runs the executable and returns the version.
     *
     * See {@link ExecUtils#parseVersionFromOutput} as a helper to implement this method.
     *
     * @return Version string.
     * @throws ConfigurationException if configuration is not correct or version could not be determined.
     *
     * @since 0.10
     */
    @Override
    protected String runExecutableAndReturnVersion() throws ConfigurationException {
        ExecUtils.parseVersionFromOutput(
            configCacheSafeOperations.execTools(),
            [executable.get().absolutePath, '-v'],
            nodeJsProjectExtension.executable.get(),
            { String content ->
                content.readLines()[0].trim()
            }) { ExecSpec spec ->
            spec.environment = NodeJSExecutor.defaultEnvironment
            spec.environment([
                npm_config_userconfig  : localConfigProvider.map { it.absolutePath },
                npm_config_globalconfig: globalConfigProvider.map { it.absolutePath }
            ])
        }
    }

    /**
     * Gets the downloader implementation.
     *
     * Can throw an exception if downloading is not supported.
     *
     * @return A downloader that can be used to retrieve an executable.
     *
     * @since 0.10
     */
    @Override
    protected ExecutableDownloader getDownloader() {
        this.npmInstaller
    }

    /**
     * Returns the name of the extension to look for when trying to find the Node.js extension.
     *
     * @return Name of Node.js project extension
     *
     * @since 0.4
     */
    protected String getNodeJsExtensionName() {
        NodeJSExtension.NAME
    }

    /**
     * Returns the name of the extension to look for when trying to find the Npm extension.
     *
     * @return Name of NPM project extension or (@code null) if this a project extension.
     *
     * @since 0.4
     */
    protected String getNpmProjectExtensionName() {
        this.npmProjectExtensionName
    }

    private BaseNpmExtension getNpmExtension() {
        ((BaseNpmExtension) projectExtension)
    }

    private final Property<File> globalConfigLocation
    private final Property<File> localConfigLocation
    private final Property<File> homeDirectoryProperty
    private final T nodeJsProjectExtension
    private final Provider<File> npxProvider
    private final NpmrcProviders npmrcProviders
    private final NpmInstaller npmInstaller
    private static final String NPX_FILENAME = 'npx-cli.js'
}