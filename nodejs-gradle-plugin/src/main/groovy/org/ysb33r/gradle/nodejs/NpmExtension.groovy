/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.nodejs

import groovy.transform.CompileStatic
import groovy.util.logging.Slf4j
import org.gradle.api.Project
import org.gradle.api.Task
import org.ysb33r.grolifant5.api.core.runnable.ProvisionedExecMethods

/** Set up global config or task-based config for NPM.
 *
 * @since 0.1
 */
@CompileStatic
@Slf4j
//class NpmExtension extends AbstractToolExtension<NpmExtension> implements ProvisionedExecMethods<NpmExecSpec> {
class NpmExtension extends BaseNpmExtension<NodeJSExtension, NpmExtension>
    implements ProvisionedExecMethods<NpmExecSpec>, NpmExtensionMethods {

    public static final String NAME = 'npm'

    /** Adds the extension to the project.
     *
     * @param project Project to link to.
     */
    NpmExtension(Project project) {
        super(
            project,
            project.extensions.getByType(NodeJSExtension)
        )
    }

    /** Adds the extension to the project.
     *
     * @param project Project to link to.
     * @param nodejs Alternative NodeJS extension to use.
     *
     * @since 0.10
     */
    NpmExtension(Project project, NodeJSExtension nodejs) {
        super(project, nodejs)
    }

    /** Adds the extension to a {@link org.ysb33r.gradle.nodejs.tasks.NpmTask} task.
     *
     * <p> Links the executable resolving to the global instance, but allows
     * it to be overridden on a per-task basis.
     *
     * @param task Task to be extended.
     */
    NpmExtension(Task task) {
        super(
            task,
            task.project.extensions.getByType(NodeJSExtension),
            task.project.extensions.getByType(NpmExtension)
        )
    }

    /** Adds the extension to a {@link org.ysb33r.gradle.nodejs.tasks.NpmTask} task.
     *
     * <p> Links the executable resolving to the global instance, but allows
     * it to be overridden on a per-task basis.
     *
     * @param task Task to be extended.
     * @param nodejs Alternative NodeJS extension to use as project extension.
     * @param altNpmExt Alternative Npm extension to use as project extension.
     *
     * @since 0.10
     */
    NpmExtension(Task task, NodeJSExtension nodejs, NpmExtension altNpmExt) {
        super(
            task,
            nodejs,
            altNpmExt
        )
    }

    /**
     * Constructs a new extension which is attached to the provided task.
     *
     * This is meant for other plugins that want to provide their own NpmExtension implementation
     *
     * @param project Project this extension is associated with.
     * @param projectExtName Name of the extension that is attached to the project.
     *
     * @since 0.4
     */
    protected NpmExtension(Task task, final String projectExtName) {
        super(
            task,
            task.project.extensions.getByType(NodeJSExtension),
            (NpmExtension) task.project.extensions.getByName(projectExtName)
        )
    }

    /** Constructs a new extension which is attached to the provided task.
     *
     * This is meant for other plugins that want to provide their own NpmExtension implementation
     * @param Task Task this extension is associated with.
     * @param extClass Type of the extension that is attached to the project.
     *
     * @since 0.10
     */
    protected NpmExtension(Task task, final Class<? super NpmExtension> extClass) {
        super(
            task,
            task.project.extensions.getByType(NodeJSExtension),
            (NpmExtension) task.project.extensions.getByType(extClass)
        )
    }
}