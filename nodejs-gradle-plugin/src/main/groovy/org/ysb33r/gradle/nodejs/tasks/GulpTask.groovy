/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.nodejs.tasks

import groovy.transform.CompileStatic
import org.gradle.api.logging.LogLevel
import org.gradle.api.provider.Property
import org.gradle.api.provider.Provider
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.Internal
import org.gradle.api.tasks.Optional
import org.ysb33r.gradle.nodejs.GulpExtension

import static java.util.Collections.EMPTY_LIST
import static java.util.Collections.EMPTY_SET

/** Ability to run a Gulp task.
 *
 *
 * @since 0.1
 */
@CompileStatic
class GulpTask extends AbstractCmdlineTask {

    GulpTask() {
        super()
        final gulpExtension = (GulpExtension) (extensions.create(GulpExtension.NAME, GulpExtension, this))

        commandProvider = nodejs.executable.map { it.absolutePath }

        final npmConfig = npm.localConfigProvider.zip(npm.globalConfigProvider) { l, g ->
            [
                npm_config_userconfig  : l.absolutePath,
                npm_config_globalconfig: g.absolutePath
            ]
        }

        environmentProvider = nodejs.environmentProvider.zip(npmConfig) { env, npmEnv ->
            env + npmEnv
        }

        addArgs(npm.npxCliJsProvider.map { File it -> it.absolutePath }, 'gulp')

        this.gulpFile = gulpExtension.gulpFileProvider
        this.gulpPath = gulpFile.map { it.absolutePath }
        this.gulpRequires = gulpExtension.requiresProvider
        this.taskToRun = project.objects.property(String)

        inputs.property('requires', this.gulpRequires)

        addArgs('--gulpfile', gulpPath)

        if (logging.level == LogLevel.QUIET) {
            addArgs('--silent')
        }
    }

    /**
     * The task to run.
     *
     * <p> {@code target} is used as a keyword as opposed to {@code task} as the latter may clash
     *   with the common DSL keyword already in used in Gradle.
     *
     * @return Task to run or {@code null} is the default task needs to be run
     */
    @Optional
    @Input
    String getTarget() {
        this.taskToRun.getOrNull()
    }

    /** Set the Gulp task to run.
     *
     * <p> {@code target} is used as a keyword as opposed to {@code task} as the latter may clash
     *   with the common DSL keyword already in used in Gradle.
     *
     * @param taskName
     */
    void setTarget(final String taskName) {
        this.taskToRun.set(taskName)
    }

    @Override
    List<String> getFinalArgs() {
        final requires = gulpRequires.getOrElse(EMPTY_SET).join(',')
        super.finalArgs +
            (requires ? ['--requires', requires] : EMPTY_LIST) +
            (taskToRun.present ? [taskToRun.get()] : EMPTY_LIST)
    }

    @Internal
    protected Provider<String> getGulpExecutablePath() {
        this.gulpExecutablePath
    }

    private final Property<String> taskToRun
    private final Provider<String> gulpPath
    private final Provider<File> gulpFile
    private final Provider<String> gulpExecutablePath
    private final Provider<Set<String>> gulpRequires
}
