/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.nodejs.tasks

import groovy.transform.CompileStatic
import org.gradle.api.provider.Provider
import org.ysb33r.gradle.nodejs.NodeJSExtension
import org.ysb33r.gradle.nodejs.NpmExtension
import org.ysb33r.gradle.nodejs.utils.npm.NpmExecutor
import org.ysb33r.gradle.nodejs.utils.npm.NpmPackageInstaller

/** Installs the packages as described by a {@code package.json} file
 *
 * @since 0.1
 */
@CompileStatic
class NpmPackageJsonInstall extends AbstractBaseNpmPackageJsonInstall<NodeJSExtension, NpmExtension> {

    NpmPackageJsonInstall() {
        super()
        final npm = extensions.create(NpmExtension.NAME, NpmExtension, this)
        final nodejs = extensions.create(NodeJSExtension.NAME, NodeJSExtension, this)
        this.env = nodejs.environmentProvider
        this.homeDir = npm.homeDirectoryProvider
        this.npmExecutor = new NpmExecutor(this, nodeExtension, npmExtension)
        this.npmPackageInstaller = createPackageInstaller()
    }

    /**
     * The NPM home directory.
     *
     * @return A provider to the home directory.
     *
     * @since 4.0
     */
    @Override
    Provider<File> getHomeDirProvider() {
        this.homeDir
    }

    @Override
    Provider<Map<String, String>> getEnvironment() {
        this.env
    }

    /**
     * Internal access to attached NPM extension.
     *
     * @return NPM extension.
     */
    @Override
    protected NpmExtension getNpmExtension() {
        extensions.getByType(NpmExtension)
    }

    /**
     * Internal access to attached NodeJS extension.
     *
     * @return NodeJS extension.
     */
    @Override
    protected NodeJSExtension getNodeExtension() {
        extensions.getByType(NodeJSExtension)
    }

    @Override
    protected NpmExecutor getNpmExecutor() {
        this.npmExecutor
    }

    /**
     * Returns the NPM packer installer instance.
     *
     * <p>
     *   This method is called from task actions and thus should be configuration cache safe.
     * </p>
     * @return Package installer.
     */
    @Override
    protected NpmPackageInstaller getNpmPackageInstaller() {
        this.npmPackageInstaller
    }

    private final NpmExecutor npmExecutor
    private final Provider<Map<String, String>> env
    private final Provider<File> homeDir
    private final NpmPackageInstaller npmPackageInstaller
}
