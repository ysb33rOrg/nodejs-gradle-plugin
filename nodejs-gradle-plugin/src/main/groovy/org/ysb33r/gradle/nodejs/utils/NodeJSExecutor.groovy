/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.nodejs.utils

import groovy.transform.CompileStatic
import org.ysb33r.gradle.nodejs.BaseNodeJSExtension
import org.ysb33r.gradle.nodejs.NodeJSExecSpec
import org.ysb33r.gradle.nodejs.NodeJSExtension

import static org.ysb33r.gradle.nodejs.internal.Downloader.OS

/** Utilities to execute Node.js scripts.
 *
 * @since 0.5
 */
@CompileStatic
class NodeJSExecutor {

    /**
     * Configures an {@link NodeJSExecSpec} from a {@link NodeJSExtension}.
     *
     * @param execSpec Execution specification to configure.
     * @param nodeJS NodeJS extension to consult during configuration
     * @return The configured execution specification.
     */
    static NodeJSExecSpec configureSpecFromExtensions(NodeJSExecSpec execSpec, NodeJSExtension nodeJS) {
        execSpec.entrypoint { executable = nodeJS.executable }
        execSpec
    }

    /**
     * Minimum default environment to use when running {@code node}
     *
     * @return Environment suitable for using in an execution specification.
     */
    @SuppressWarnings('UnnecessaryCast')
    static Map<String, Object> getDefaultEnvironment() {
        if (OS.windows) {
            [
                TEMP        : System.getenv('TEMP'),
                TMP         : System.getenv('TMP'),
                (OS.pathVar): System.getenv(OS.pathVar)
            ] as Map<String, Object>
        } else {
            [(OS.pathVar): System.getenv(OS.pathVar)] as Map<String, Object>
        }
    }

    /**
     * Creates an {@link org.ysb33r.gradle.nodejs.NodeJSExecSpec} with default {@code node} location.
     *
     * @param nodeJS Node extension
     * @return Execution specification.
     *
     * @since 2.0
     */
    static <T extends BaseNodeJSExtension<T>> NodeJSExecSpec createNodeJSExecSpec(T nodeJS) {
        nodeJS.createExecSpec()
    }

    /**
     * Creates an {@link org.ysb33r.gradle.nodejs.NodeJSExecSpec} with default {@code node} location and a default
     * environment.
     *
     * @param nodeJS Node extension
     * @return Execution specification.
     *
     * @since 2.0
     */
//    static <T extends BaseNodeJSExtension<T>> NodeJSExecSpec createNodeJSExecSpecWithDefaultEnvironment(T nodeJS) {
    static <T extends BaseNodeJSExtension<T>> NodeJSExecSpec createNodeJSExecSpecWithDefaultEnvironment(T nodeJS) {
        final spec = createNodeJSExecSpec(nodeJS)
        final env = defaultEnvironment
        final nodePath = nodeJS.executable.map { f -> f.parentFile.absolutePath }
        if (env.containsKey(OS.pathVar)) {
            env[OS.pathVar] = "${nodePath}${OS.pathSeparator}${env[OS.pathVar]}"
        } else {
            env[OS.pathVar] = nodePath
        }
        spec.entrypoint {
            environment = env
        }
        spec
    }
}
