/*
 * ============================================================================
 * (C) Copyright Schalk W. Cronje and/or respective authors identified in
 * documentation sections 2016 - 2025
 *
 * This software is licensed under the Apache License 2.0
 * See http://www.apache.org/licenses/LICENSE-2.0 for license details
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ============================================================================
 */
package org.ysb33r.gradle.nodejs.tasks

import groovy.transform.CompileStatic
import org.gradle.api.provider.Property
import org.gradle.api.provider.Provider
import org.gradle.api.tasks.Internal
import org.gradle.api.tasks.TaskProvider
import org.ysb33r.gradle.nodejs.internal.Transform
import org.ysb33r.grolifant5.api.core.OperatingSystem
import org.ysb33r.grolifant5.api.core.wrappers.AbstractWrapperGeneratorTask

/**
 * A task to generate Node wrappers.
 *
 * @author Schalk W. Cronjé
 */
@CompileStatic
class NodeWrappers extends AbstractWrapperGeneratorTask {

    public static final String NODE_WRAPPER = 'nodew'
    public static final String NODE_WRAPPER_WINDOWS = 'nodew.bat'

    @SuppressWarnings('UnnecessaryCast')
    NodeWrappers() {
        super()
        useWrapperTemplatesInResources(
            '/gradle-node-wrappers',
            MAPPING
        )

        this.locationPropertiesFile = project.objects.property(File)
        this.cacheTaskName = project.objects.property(String)

        this.tokenValues = locationPropertiesFile.zip(cacheTaskName) { lpf, cacheTaskName ->
            [
                APP_BASE_NAME               : 'node',
                APP_LOCATION_FILE           : lpf.name,
                CACHE_TASK_NAME             : cacheTaskName,
                GRADLE_WRAPPER_RELATIVE_PATH: wrapperDestinationDirProvider.map {
                    fsOperations().relativePathToRootDir(it) ?: CURRENT_DIR
                }.get(),
                DOT_GRADLE_RELATIVE_PATH    : wrapperDestinationDirProvider.map {
                    fsOperations().relativize(it, fsOperations().projectCacheDir) ?: CURRENT_DIR
                }.get()

            ] as Map<String, String>
        }

        outputs.files(Transform.toList((Collection) MAPPING.values()) { String it ->
            wrapperDestinationDirProvider.map { File f -> new File(f, (String) it) }
        })

        this.nodeWrapperProvider = wrapperDestinationDirProvider.map {
            OperatingSystem.current().windows ? new File(it, NODE_WRAPPER_WINDOWS) : new File(it, NODE_WRAPPER)
        }
    }

    void associateCacheTask(TaskProvider<NodeBinariesCacheTask> ct) {
        inputs.files(ct)
        this.locationPropertiesFile.set(ct.flatMap { it.locationPropertiesFile })
        this.cacheTaskName.set(ct.map { it.name })
    }

    @Internal
    Provider<File> getNodeWrapperProvider() {
        this.nodeWrapperProvider
    }

    @Override
    protected String getBeginToken() {
        '~~'
    }

    @Override
    protected String getEndToken() {
        '~~'
    }

    @Override
    protected Map<String, String> getTokenValuesAsMap() {
        this.tokenValues.get()
    }

    private final Provider<Map<String, String>> tokenValues
    private final Provider<File> nodeWrapperProvider
    private final Property<File> locationPropertiesFile
    private final Property<String> cacheTaskName

    private static final Map<String, String> MAPPING = [
        'node-wrapper-template.sh' : NODE_WRAPPER,
        'node-wrapper-template.bat': NODE_WRAPPER_WINDOWS,
        'npm-wrapper-template.sh'  : 'npmw',
        'npm-wrapper-template.bat' : 'npmw.bat',
        'npx-wrapper-template.sh'  : 'npxw',
        'npx-wrapper-template.bat' : 'npxw.bat'
    ]

    private static final String CURRENT_DIR = '.'
}
